{
  cfg,
  config,
  lib,
  pkgs,
  ...
}:
let
  toImv =
    with lib.generators;
    attrs:
    toINI { } { inherit (attrs) options; }
    + toINI { } { inherit (attrs) aliases; }
    + toINI { } { inherit (attrs) binds; };

  inherit (config.definitions) theme fonts;

in
{
  options.enable = lib.mkEnableOption "zitrones imv config";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      trash-cli
      imv
    ];

    environment.etc."imv_config".text = toImv {
      options = {
        background = theme.background.rgbHex;
        overlay = true;
        overlay_font = "${fonts.sansSerif.name}:${toString fonts.sansSerif.size}";
        overlay_text = builtins.concatStringsSep " " [
          "[$imv_current_index/$imv_file_count]"
          "\${imv_current_file##*/} [$imv_width\\x$imv_height]"
          "Zoom: $imv_scale\\%"
          ''$(test $imv_loading -eq 1 && echo "(loading)")''
        ];
        overlay_text_color = theme.foreground.rgbHex;
        overlay_background_color = theme.background.rgbHex;
        overlay_background_alpha = "CC";
        suppress_default_binds = true;
      };
      aliases = {
        delete = ''exec rm "$imv_current_file"'';
        trash = ''exec ${lib.getExe' pkgs.trash-cli "trash-put"} "$imv_current_file"'';
      };
      binds = {
        "<Ctrl+Left>" = "goto 1";
        "<Ctrl+Right>" = "goto -1";
        "<Left>" = "prev";
        "<Right>" = "next";
        "<Up>" = "zoom 1";
        "<Down>" = "zoom -1";
        "<Ctrl+t>" = "goto 1";
        "<Ctrl+e>" = "goto -1";
        "<t>" = "prev";
        "<e>" = "next";
        "<u>" = "zoom 1";
        "<i>" = "zoom -1";
        "<period>" = "next_frame";
        "<space>" = "toggle_playing";
        "dd" = "trash; next";
        "Dd" = "delete; next";
        "o" = "overlay";
        "c" = "scaling full";
        "r" = "rotate by 90";
        "j" = "rotate by 90";
        "v" = "flip vertical";
        "f" = "fullscreen";
        "q" = "quit";
        "<Escape>" = "quit";
      };
    };
  };
}
