{
  cfg,
  pkgs,
  lib,
  ...
}:
{
  options.enable = lib.mkEnableOption "zitrones shell agnostic shell configs";

  config = lib.mkIf cfg.enable {

    # additional useful commandline utilities
    environment.systemPackages = with pkgs; [
      # networking
      wget
      curl
      dig
      # general cli tools/rust rewrites
      bat
      bat-extras.prettybat
      rename
      moreutils
      gavin-bc
      dust
      duf
      hyperfine
      # data parsing
      jq
      yq
      # as tldr client
      tealdeer
      # more man pages
      man-pages
      man-pages-posix
    ];
    # even more documentation
    documentation = {
      dev.enable = true;
      info.enable = true;
      man.generateCaches = false;
    };
    environment.variables = {
      BAT_THEME = "base16";
      BAT_STYLE = "plain";
      PAGER = "less";
      SYSTEMD_LESS = "FRMK";
      # colored manpages
      MANLESS =
        # for whatever reason needed for it to work
        "$"
        # magenta headings (bold text)
        + " -Dd+m"
        # green commands (underlined text)
        + " -Du+G"
        # for whatever reason needed for it to work
        + " --use-color"
        # black text on bright red background for search results
        + " -DSkR"
        # uncolored prompt
        + " -DP--"
        # grey line numbers (not used unless specifically enabled with MANLESS="$MANLESS -N")
        + " -DNK";
      MANROFFOPT = "-c";
    };
    environment.shellAliases = {
      # ls shortcuts
      l = "ls --hyperlink --color --human-readable --group-directories-first"; # short
      ll = "ls --hyperlink --color --human-readable --group-directories-first -l -v --time-style long-iso"; # long
      la = "ls --hyperlink --color --human-readable --almost-all"; # all
      lla = "ls --hyperlink --color --human-readable --almost-all -l -v --time-style long-iso"; # all long
      # useful shortcuts
      # clear but it force clears the scrollback buffer too
      cl = ''printf "\033[2J\033[3J\033[1;1H"'';
      echonl = "printf '%s\n'";
      # safety shortcuts
      rename = "rename -bv";
      mv = "mv -bv";
      cp = "cp -bv";
      rsync = "echo NO!; false";
      # remember to use alternative tools
      multitime = "echo use hyperfine";
      du = "echo use dust";
      df = "echo use duf";
    };

    # use posix compliant shell for /bin/sh
    environment.binsh = "${pkgs.dash}/bin/dash";
  };
}
