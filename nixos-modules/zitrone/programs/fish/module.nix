{
  cfg,
  pkgs,
  lib,
  ...
}:
{
  options.enable = lib.mkEnableOption "zitrones fish shell config";

  config = lib.mkIf cfg.enable {
    zitrone.programs.shell-base.enable = true;

    programs.fish = {
      enable = true;
      useBabelfish = true;
      shellInit = builtins.readFile ./config.fish;
      promptInit = builtins.readFile ./prompt.fish;
      interactiveShellInit = ''
        function fish_greeting
          ${pkgs.writeShellScript "nixos-mini-multiple-rainbow" (
            builtins.readFile ./nixos-mini-multiple-rainbow.sh
          )}
        end
      '';
    };

    environment.systemPackages = with pkgs.fishPlugins; [
      pisces
      puffer
      done
    ];

    environment.variables = {
      __done_min_cmd_duration = "15000";
      __done_notify_sound = "0";
    };
  };
}
