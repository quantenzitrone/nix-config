function fish_prompt --description 'Write out the prompt'
    # get status
    set -lx last_status $status

    # shortcuts for colors
    set -lx col_normal (set_color normal)
    set -lx col_vcs (set_color brpurple)

    # different suffix and color if root
    set -lx col_cwd (set_color green)
    set -lx suffix '$'
    if functions -q fish_is_root_user; and fish_is_root_user
        set col_cwd (set_color red)
        set suffix '#'
    end

    # prompt status on failed command
    set -lx prompt_status ""
    set -lx col_status (set_color brgreen)
    if test $last_status -ne 0
        set prompt_status "[$last_status]"
        set col_status (set_color brred)
    end

    # nix shell info
    set -lx prompt_nix_shell ""
    if test -n "$IN_NIX_SHELL"
        set prompt_nix_shell " ( $IN_NIX_SHELL)"
    end

    # function for printing the first line of the prompt
    function prompt_line --description 'Print the informational prompt line'
        echo "$(prompt_login) $col_cwd$(prompt_pwd)$col_vcs$(fish_vcs_prompt)$prompt_nix_shell$col_normal $col_status$prompt_status"
    end

    set -lx fish_prompt_pwd_dir_length 0
    # test if the prompt would be too long
    if test (tput cols) -lt (string length --visible (prompt_line))
        set fish_prompt_pwd_dir_length 1
    end

    # print prompt
    prompt_line
    echo -n -s $col_status $suffix $col_normal ' '
end
