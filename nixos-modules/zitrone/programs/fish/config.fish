#!/usr/bin/env fish

# make a directory and directly cd into it
function mkcd --wraps mkdir
    if test -e $argv[1]
        if test -d $argv[1]
            echo -s \
                (set_color yellow) "warning:" (set_color normal) " " \
                (set_color blue) $argv[1] (set_color normal) \
                " already exists" >&2
            cd $argv[1]
        else
            echo -s \
                (set_color red) "error:" (set_color normal) " " \
                (set_color blue) $argv[1] (set_color normal) \
                " already exists and is not a directory" >&2
            return 1
        end
    else
        mkdir -p $argv[1]
        cd $argv[1]
    end
end

# cd into the parent directory and remove the current directory
function cdrmdir
    set dir (pwd)
    cd ..
    rmdir $dir
end

# nix wrapper that allows everything: broken unsupported_system unfree and insecure
function nix! --wraps nix
    # enable all nix packages
    set -lx NIXPKGS_ALLOW_BROKEN 1
    set -lx NIXPKGS_ALLOW_UNSUPPORTED_SYSTEM 1
    set -lx NIXPKGS_ALLOW_UNFREE 1
    set -lx NIXPKGS_ALLOW_INSECURE 1
    # execute command with --impure flag
    if set -l pos (contains -i -- -- $argv)
        nix $argv[..(math $pos-1)] --impure $argv[$pos..]
    else
        nix $argv --impure
    end
end

# prints the store path of the derivation containing the given binaries
function which-nix --wraps which
    path resolve (which $argv)/../..
end

# open all given files in the background with xdg-open
function open --wraps xdg-open
    for i in $argv
        xdg-open $i &
    end
end

# keyboard shortcuts
bind \cH backward-kill-path-component
bind \cW ""
