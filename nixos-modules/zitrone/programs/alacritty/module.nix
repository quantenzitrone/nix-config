{
  cfg,
  config,
  lib,
  pkgs,
  ...
}:
{
  options.enable = lib.mkEnableOption "zitrones alacritty config";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = [ pkgs.alacritty ];
    environment.etc."xdg/alacritty/alacritty.toml".source = import ./alacritty.toml.nix {
      inherit lib pkgs config;
    };
  };
}
