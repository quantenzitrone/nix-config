{
  lib,
  pkgs,
  config,
}:
let
  inherit (config.definitions.theme)
    background
    foreground
    termcolors
    ;
in
pkgs.writers.writeTOML "alacritty.toml" {
  env = {
    # make font size on wayland the same as on xorg
    WINIT_X11_SCALE_FACTOR = "1.0";
  };
  window = {
    padding = {
      x = 5;
      y = 5;
    };
    opacity = background.aFloat;
  };

  font = {
    size = config.definitions.fonts.monospace.size;
  };

  colors = {
    primary = {
      background = "#" + background.rgbHex;
      foreground = "#" + foreground.rgbHex;
    };
    normal = lib.mapAttrs (_: v: "#" + v.rgbHex) termcolors.normal;
    bright = lib.mapAttrs (_: v: "#" + v.rgbHex) termcolors.bright;
    cursor.cursor = "#" + foreground.rgbHex;
  };

  cursor.style.shape = "Beam";
}
