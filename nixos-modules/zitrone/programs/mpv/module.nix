{
  cfg,
  config,
  lib,
  pkgs,
  ...
}:
{
  options = {
    enable = lib.mkEnableOption "zitrones mpv config";
    screenshotPath = lib.mkOption { type = lib.types.str; };
  };

  config = lib.mkIf cfg.enable {
    environment.variables.MPV_HOME = "/etc/xdg/mpv";
    environment.systemPackages = with pkgs; [ mpv ];
    environment.etc = {
      "xdg/mpv/input.conf".text = import ./input.conf.nix {
        inherit lib;
      };
      "xdg/mpv/mpv.conf".text = import ./mpv.conf.nix {
        inherit config lib;
        inherit (cfg) screenshotPath;
      };
      "xdg/mpv/script-opts/osc.conf".source = ./script-opts/osc.conf;
      "xdg/mpv/script-opts/stats.conf".source = ./script-opts/stats.conf;
      "xdg/mpv/script-opts/console.conf".source = ./script-opts/console.conf;
      "xdg/mpv/script-opts/ytdl_hook.conf".source = ./script-opts/ytdl_hook.conf;
    };
  };
}
