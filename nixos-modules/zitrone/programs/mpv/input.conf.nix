{ lib }:
let
  inherit (builtins) concatStringsSep;

  repeat = x: y: lib.replicate x y |> concatStringsSep "; ";

  mkConf =
    x:
    lib.mapAttrsRecursive (path: value: "${concatStringsSep "+" path} ${value}") x
    |> lib.collect (x: !lib.isAttrs x)
    |> concatStringsSep "\n";
in
mkConf {
  # volume control
  WHEEL_UP = "add volume 1";
  WHEEL_DOWN = "add volume -1";
  UP = "add volume 1";
  DOWN = "add volume -1";
  Ctrl.WHEEL_UP = "add volume 10";
  Ctrl.WHEEL_DOWN = "add volume -10";
  Ctrl.UP = "add volume 10";
  Ctrl.DOWN = "add volume -10";
  m = "cycle mute";

  # seeking
  "." = "frame-step";
  "," = "frame-back-step";
  RIGHT = "seek 10";
  LEFT = "seek -5";
  WHEEL_RIGHT = "seek 10";
  WHEEL_LEFT = "seek -5";
  Ctrl.RIGHT = "seek 100";
  Ctrl.LEFT = "seek -50";
  Ctrl.WHEEL_RIGHT = "seek 100";
  Ctrl.WHEEL_LEFT = "seek -50";
  Alt.RIGHT = "seek 2";
  Alt.LEFT = "seek -1";
  Alt.WHEEL_RIGHT = "seek 2";
  Alt.WHEEL_LEFT = "seek -1";

  # playlist control
  MBTN_FORWARD = "playlist-next";
  MBTN_BACK = "playlist-prev";
  NEXT = "playlist-next";
  PREV = "playlist-prev";
  ">" = "playlist-next";
  "<" = "playlist-prev";
  Ctrl.MBTN_FORWARD = repeat 5 "playlist-next";
  Ctrl.MBTN_BACK = repeat 5 "playlist-prev";
  Ctrl.NEXT = repeat 5 "playlist-next";
  Ctrl.PREV = repeat 5 "playlist-prev";
  Ctrl.">" = repeat 5 "playlist-next";
  Ctrl."<" = repeat 5 "playlist-prev";

  # playback speed
  "]" = "multiply speed 1.4142135623730951"; # 2^0.5
  "[" = "multiply speed 0.7071067811865475";
  Ctrl."]" = "multiply speed 2";
  Ctrl."[" = "multiply speed 0.5";
  Alt."]" = "multiply speed 1.0905077326652577"; # 2^0.125
  Alt."[" = "multiply speed 0.9170040432046712";

  # subtitle delay
  ")" = "add sub-delay 1";
  "(" = "add sub-delay -1";
  Ctrl.")" = "add sub-delay 10";
  Ctrl."(" = "add sub-delay -10";
  Alt.")" = "add sub-delay 0.1";
  Alt."(" = "add sub-delay -0.1";

  # chapter control
  c = "add chapter +1";
  C = "add chapter -1";

  # playpause
  SPACE = "cycle pause";
  MBTN_LEFT = "cycle pause";
  PLAY = "cycle pause";

  # fullscreen
  f = "cycle fullscreen";
  MBTN_LEFT_DBL = "cycle fullscreen";
  ESC = "set fullscreen no";

  # channel control
  v = "cycle video";
  V = "cycle video down";
  a = "cycle audio";
  A = "cycle audio down";
  s = "cycle sub";
  S = "cycle sub down";

  # help
  h = ''show-text "${
    concatStringsSep "\\n" [
      "h : show this help"
      "v/V : next/prev video track"
      "a/A : next/prev audio track"
      "s/S : next/prev subtitle track"
      "c/C : next/prev chapter"
      ">/< : next/prev playlist item"
      "() : change subtitle delay"
      "[] : change playback speed"
      "← → : seek by time"
      ",. : seek by frame"
      "↑↓ : volume control"
      "m : toggle mute"
      ": : show console"
      "? : show stats"
      "Alt : more precision modifier"
      "Ctrl : less precision modifier"
    ]
  }" 5000'';

  # other
  ":" = "script-binding console/enable";
  "?" = "script-binding stats/display-stats-toggle";
  q = "quit";
}
