{
  lib,
  screenshotPath,
  config,
}:
let
  str = x: "'${x}'";
  inherit (config.definitions.theme) background;
in
lib.generators.toKeyValue { } {
  # font
  osd-font = str "sans-serif";
  sub-font = str "sans-serif";
  osd-font-size = 22;
  # screenshot configuration
  screenshot-format = "png";
  screenshot-template = str "%F-%p";
  screenshot-png-compression = 7;
  screenshot-directory = screenshotPath;

  # transparency
  force-window = "immediate";
  background = "color";
  background-color = str "#${background.aHex}${background.rgbHex}";
  # replaygain
  replaygain = "track";
}
# disable default keybindings
+ ''
  no-input-default-bindings
''
