{
  cfg,
  lib,
  pkgs,
  ...
}:
let
  inherit (lib) mkEnableOption mkIf;
in
{
  options.enable = mkEnableOption "zitrones cosmic desktop configuration";

  config = mkIf cfg.enable {
    services.desktopManager.cosmic.enable = true;

    environment.cosmic.excludePackages = with pkgs; [
      cosmic-term
      cosmic-player
    ];

    environment.systemPackages = with pkgs; [
      # applets
      cosmic-ext-applet-clipboard-manager
      cosmic-ext-applet-emoji-selector
      cosmic-ext-applet-external-monitor-brightness
      # other
      andromeda
      chronos
      cosmic-ext-calculator
      cosmic-ext-ctl
      examine
      forecast
      observatory
      cosmic-ext-tweaks
      quick-webapps
      tasks
    ];

    zitrone.programs.alacritty.enable = true;

    zitrone.programs.mpv.enable = true;
  };
}
