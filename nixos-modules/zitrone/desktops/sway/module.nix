{
  cfg,
  lib,
  pkgs,
  ...
}:
{
  options = {
    enable = lib.mkEnableOption "zitrones sway desktop";
    users = lib.mkOption {
      description = "list of users to enable the home-manager dependant configs for";
      type = with lib.types; listOf str;
      default = [ ];
      example = [ "alice" ];
    };
  };

  config = lib.mkIf cfg.enable {
    programs.sway = {
      enable = true;
      #package = pkgs.swayfx; # TODO remove
      wrapperFeatures.base = true;
      wrapperFeatures.gtk = true;
      extraOptions = [ "--unsupported-gpu" ];
    };

    services.dbus.enable = true;

    xdg.portal = {
      enable = true;
      wlr.enable = true;
      wlr.settings = {
        screencast = {
          output_name = "WAYLAND-OUTPUT";
          max_fps = 60;
          exec_before = "${pkgs.dunst}/bin/dunstctl set-paused true";
          exec_after = "${pkgs.dunst}/bin/dunstctl set-paused false";
          chooser_type = "simple";
          chooser_cmd = "${pkgs.slurp}/bin/slurp -f %o -or";
        };
      };
      extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };

    home-manager.users = lib.genAttrs cfg.users (_: {
      zitrone = {
        desktops.sway = {
          enable = true;
        };
      };
      # use sway nixos module
      wayland.windowManager.sway.package = null;
    });
  };
}
