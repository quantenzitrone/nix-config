{
  cfg,
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (config.definitions.theme) background foreground accent;
  inherit (config.definitions.theme.termcolors.normal) red;

  swaycfg = pkgs.writeText "gtty-sway-config" ''
    input type:keyboard {
      xkb_layout ${config.services.xserver.xkb.layout}
      xkb_variant ${config.services.xserver.xkb.variant}
      xkb_options ${config.services.xserver.xkb.options}
    }

    input type:touchpad {
      tap enabled
      natural_scroll enabled
      tap_button_map lrm
      middle_emulation enabled
      drag enabled
    }

    # <colorclass>          <border> <background> <text> <indicator> <child_border>
    client.background       #${background.rgbHex}
    client.focused          #${accent.rgbHex} #${accent.rgbHex} #${foreground.rgbHex} #${accent.rgbHex} #${accent.rgbHex}
    client.focused_inactive #${accent.rgbHex} #${accent.rgbHex} #${foreground.rgbHex} #${accent.rgbHex} #${accent.rgbHex}
    client.placeholder      #${background.rgbHex} #${background.rgbHex} #${foreground.rgbHex} #${background.rgbHex} #${background.rgbHex}
    client.unfocused        #${background.rgbHex} #${background.rgbHex} #${foreground.rgbHex} #${background.rgbHex} #${background.rgbHex}
    client.urgent           #${red.rgbHex} #${red.rgbHex} #${foreground.rgbHex} #${red.rgbHex} #${red.rgbHex}

    default_border none
    workspace_layout tabbed
    hide_edge_borders --i3 smart

    bindsym Mod4+Tab focus next
    bindsym Mod4+Shift+Tab focus prev

    exec_always --no-startup-id ${pkgs.writeShellScript "systemd-gtty" ''
      ${pkgs.dbus}/bin/dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY I3SOCK SWAYSOCK XDG_CURRENT_DESKTOP
      ${pkgs.systemd}/bin/systemctl --user start gtty-session.target
    ''}

    exec "${lib.getExe cfg.terminal}; swaymsg exit"
  '';
in
{
  options = {
    enable = lib.mkEnableOption "GUItty - single window terminal wayland compositor";
    terminal = lib.mkOption {
      type = lib.types.package;
      default = pkgs.alacritty;
      description = "the terminal to display as a single window";
    };
    users = lib.mkOption {
      description = "list of users to enable the home-manager dependant configs for";
      type = with lib.types; listOf str;
      default = [ ];
      example = [ "alice" ];
    };
  };

  config = lib.mkIf cfg.enable {
    programs.sway.enable = true;

    systemd.user.targets.gtty-session = {
      description = "to start gtty, a sway powered graphical tty";
      wants = [ "graphical-session.target" ];
      after = [ "graphical-session.target" ];
    };

    home-manager.users = lib.genAttrs cfg.users (_: {
      zitrone.services.wpaperd = {
        enable = true;
        startOn = [ "gtty-session.target" ];
      };
    });

    environment.shellAliases.gtty = "sway --config ${swaycfg}";
  };
}
