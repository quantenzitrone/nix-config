{ cfg, lib, ... }:
{
  options = {
    enable = lib.mkEnableOption "zitroness i3 desktop";
    users = lib.mkOption {
      description = "list of users to enable the home-manager dependant configs for";
      type = with lib.types; listOf str;
      default = [ ];
      example = [ "alice" ];
    };
  };

  config = lib.mkIf cfg.enable {
    # enable i3 window manager
    services.xserver.enable = true;
    services.xserver.displayManager.sx.enable = true;
    environment.shellAliases.starti3 = "sx env XDG_CURRENT_DESKTOP=i3 i3";

    services.xserver.windowManager.i3.enable = true;

    home-manager.users = lib.genAttrs cfg.users (_: {
      zitrone = {
        desktops.i3 = {
          enable = true;
        };
      };
    });
  };
}
