{
  cfg,
  lib,
  pkgs,
  ...
}:
with lib;
let
  inherit (lib.strings) normalizePath;
  serviceName = "symlinks";

  # the options for each symlink
  symlinkOptions = {
    options = {
      target = mkOption {
        type = with types; nullOr path;
        default = null;
        example = "/data/Documents";
        description = "the target file/folder to link to";
      };
      force = mkOption {
        type = types.bool;
        default = false;
        example = true;
        description = "remove existing destination files (dangerous)";
      };
    };
  };

  # template for a systemd service to do the symlinking
  symlinkService =
    symlinks:
    let
      linkerScript = pkgs.writeShellApplication {
        name = "linker";
        runtimeInputs = with pkgs; [
          stackfile
          libnotify
          jq
        ];
        text = builtins.readFile ./linker.sh;
      };
    in
    {
      wantedBy = [ "default.target" ];
      environment = {
        HOME = "%h";
        appname = "${serviceName}.service";
        restartmsg = "restart the service with 'systemctl --user restart ${serviceName}.service'";
        stackfile = "%h/.local/state/symlinks.stackfile";
      };
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = "yes";
        ExecStart = "${linkerScript}/bin/linker link ${pkgs.writeText "${serviceName}" (builtins.toJSON symlinks)}";
        ExecStop = "${linkerScript}/bin/linker unlink";
      };
    };
in
{
  options = mkOption {
    type = with types; attrsOf (submodule symlinkOptions);
    default = { };
    example = literalExpression ''
      {
        ".config/someprogram" = {
          target = "/etc/xdg/someprogram";
          force = true;
        };
      }
    '';
    description = "Create symlinks in each users home directories to files and folders in different locations.";
  };

  config.systemd.user.services = mkIf (cfg != { }) {
    "${serviceName}" = symlinkService (
      mapAttrsToList (name: value: {
        linkName = pipe name [
          normalizePath
          (removeSuffix "/")
        ];
        target = pipe value.target [
          normalizePath
          (removeSuffix "/")
        ];
        inherit (value) force;
      }) cfg
    );
  };
}
