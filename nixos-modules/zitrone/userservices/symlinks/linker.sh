#!/usr/bin/env bash

# error
function error() {
	printf "\033[91m"
	echo -n "error:"
	printf "\033[0m"
	echo " $*"
}

# warning
function warning() {
	printf "\033[91m"
	echo -n "warning:"
	printf "\033[0m"
	echo " $*"
}

# send error with notify send as well
function notifyerror() {
	error "$*"
	notify-send "$appname" "$*"
}

# overridable app name
appname="${appname:-"$0"}"

# overridable message to rerun the script
restartmsg="${restartmsg:-"rerun this script with '$0'"}"

# overridable position for the existing symlinks stackfile
stackfile="${stackfile:-"/tmp/symlinks.stackfile"}"
mkdir -p "$(dirname "$stackfile")"
touch "$stackfile"

# test validity of the command line arguments
if test "${1:-}" = link; then
	if test -f "${2:-}"; then
		if ! jq -e . "${2:-}" >/dev/null 2>&1; then
			error the specified file is not valid json
			exit 1
		fi
	else
		error the file specified as \$2 is not a regular file
		exit 1
	fi
	link=true
elif test "${1:-}" = unlink; then
	link=false
else
	error you have to specify either link or unlink with \$1
	exit 1
fi

# do the thing
if $link; then
	$0 unlink
	failed=false
	for entry in $(jq -c '.[]' "${2:-}"); do
		target="$(jq -r '.target' <<<"$entry")"
		linkName="$(jq -r '.linkName' <<<"$entry")"
		# cannot link to non existing target
		if ! test -e "$target"; then
			notifyerror "cannot create link to target file $target: file doesn't exist"
			continue
			failed=true
		fi
		mkdir -p "$(dirname "$linkName")"
		# link the files
		if jq -e '.force' <<<"$entry" >/dev/null; then
			# warn if file gets removed
			if test -e "$linkName"; then
				warning "$linkName has been removed because force was set to true"
			fi
			ln --force -sT "$target" "$linkName"
		else
			# don't remove existing file at linkName
			if test -e "$linkName"; then
				notifyerror "cannot create link with name $linkName, file already exists"
				continue
				failed=true
			fi
			ln -sT "$target" "$linkName"
		fi
		stackfile "$stackfile" push "$linkName"
	done
	if $failed; then
		notify-send "$appname" "$(
			echo "errors have occured:"
			echo " (re-)move existing destination files and/or create missing target files to fix the errors."
			echo " Then $restartmsg to create the missing symlinks"
		)"
	fi
else
	while ! stackfile "$stackfile" empty; do
		linkName="$(stackfile "$stackfile" pop)"
		if test -L "$linkName"; then
			unlink "$linkName"
		else
			warning "not removing non-symlink $linkName"
		fi
	done
fi
