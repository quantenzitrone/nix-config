{
  cfg,
  options,
  pkgs,
  lib,
  ...
}:
let
  inherit (import ../actkbd/lib.nix { inherit lib pkgs; }) mkConfigFile;
  bindings = [
    /*
      29     Ctrl
      42     LShift
      56     RShift
      125    Meta
      56     Alt
      165 67 Prev / f11
      164 68 PlayPause / f9
      163 87 Next / f10
    */
    # play/pause
    {
      keys = [ 164 ];
      events = [ "key" ];
      command = "${pkgs.mpc-cli}/bin/mpc toggle";
    }
    # next
    {
      keys = [ 163 ];
      events = [ "key" ];
      command = "${pkgs.mpc-cli}/bin/mpc next";
    }
    # prev
    {
      keys = [ 165 ];
      events = [ "key" ];
      command = "${pkgs.mpc-cli}/bin/mpc prev";
    }
    # seek backwards
    {
      keys = [
        29
        67
      ];
      events = [ "key" ];
      attributes = [
        "all"
        "exec"
      ];
      command = "${pkgs.mpc-cli}/bin/mpc seek -10";
    }
    # seek forward
    {
      keys = [
        29
        87
      ];
      events = [ "key" ];
      attributes = [
        "all"
        "exec"
      ];
      command = "${pkgs.mpc-cli}/bin/mpc seek +10";
    }
    # stop playback
    {
      keys = [
        29
        68
      ];
      events = [ "key" ];
      attributes = [
        "all"
        "exec"
      ];
      command = "${pkgs.mpc-cli}/bin/mpc clear";
    }
  ];
in
{
  options = {
    enable = lib.mkEnableOption "mpd with actkbd global keyboard shortcuts";
    inherit (options.services.mpd) musicDirectory playlistDirectory;
    user = lib.mkOption {
      default = null;
      type = with lib.types; nullOr str;
    };
  };

  config = lib.mkIf cfg.enable {
    zitrone.services.pipewire.enable = true;

    # needed packages
    environment.systemPackages = with pkgs; [
      mpd
      mpc-cli
      ymuse
      listenbrainz-mpd
    ];

    # mpd
    services.mpd = {
      enable = true;
      inherit (cfg) musicDirectory playlistDirectory;
      extraConfig = ''
        audio_output {
          type "pipewire"
          name "mpd"
        }
        replaygain "auto"
      '';
    };

    # set the selected user as the mpd user and make sure it has the required groups
    systemd.services.mpd.serviceConfig.User = lib.mkIf (cfg.user != null) (lib.mkOverride 90 cfg.user);
    users.users = lib.mkIf (cfg.user != null) {
      ${cfg.user}.extraGroups = [
        "audio"
        "pipewire"
      ];
    };

    systemd.services.mpd-listenbrainz =
      let
        configFile = "/etc/xdg/listenbrainz-mpd/config.toml";
      in
      {
        description = "ListenBrainz submission client for MPD";
        documentation = [
          "https://codeberg.org/elomatreb/listenbrainz-mpd"
          "man:listenbrainz-mpd(1)"
        ];
        after = [ "mpd.service" ];
        wantedBy = [ "mpd.service" ];
        serviceConfig = {
          Type = "notify";
          Restart = "always";
          RestartSec = 5;
          ExecStart = "${lib.getExe pkgs.listenbrainz-mpd} --config ${configFile}";
        };
      };

    # keyboard shortcuts
    services.udev.packages = lib.singleton (
      pkgs.writeTextFile {
        name = "actkbd-mpd-udev-rules";
        destination = "/etc/udev/rules.d/61-actkbd-mpd.rules";
        text = builtins.concatStringsSep ", " [
          ''ACTION=="add"''
          ''KERNEL=="event[0-9]*"''
          ''SUBSYSTEM=="input"''
          ''ENV{ID_INPUT_KEY}=="1"''
          ''ENV{ID_PATH}=="platform-i8042-serio-0"''
          ''TAG+="systemd"''
          ''ENV{SYSTEMD_WANTS}+="actkbd-mpd@$env{DEVNAME}.service"''
        ];
      }
    );
    systemd.services."actkbd-mpd@" = {
      after = [ "mpd.service" ];

      restartIfChanged = true;
      unitConfig = {
        Description = "actkbd on %I for mpd control";
        ConditionPathExists = "%I";
      };

      serviceConfig = {
        Type = "forking";
        ExecStart = "${pkgs.actkbd}/bin/actkbd -D -c ${mkConfigFile bindings} -d %I";

        # hardening

        ## allow access to event device
        DeviceAllow = [ "%I" ];
        DevicePolicy = "closed";

        ## specifically needed
        RestrictAddressFamilies = [ "AF_INET" ];
        IPAddressAllow = "localhost";

        CapabilityBoundingSet = "";
        IPAddressDeny = "any";
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        NoNewPrivileges = true;
        PrivateTmp = true;
        PrivateUsers = true;
        ProtectClock = true;
        ProtectControlGroups = true;
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        ProtectProc = "invisible";
        ProtectSystem = "strict";
        RestrictNamespaces = true;
        RestrictRealtime = true;
        RestrictSUIDSGID = true;
        SocketBindDeny = "any";
        SystemCallArchitectures = "native";
        SystemCallFilter = "~@aio:EPERM @chown:EPERM @clock:EPERM @cpu-emulation:EPERM @debug:EPERM @ipc:EPERM @keyring:EPERM @memlock:EPERM @module:EPERM @mount:EPERM @obsolete:EPERM @pkey:EPERM @privileged:EPERM @raw-io:EPERM @reboot:EPERM @resources:EPERM @sandbox:EPERM @setuid:EPERM @swap:EPERM @sync:EPERM @timer:EPERM";
        UMask = "0077";

        # -> 1.5 OK
      };
    };
  };
}
