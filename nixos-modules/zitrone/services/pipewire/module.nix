{
  cfg,
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (import ../actkbd/lib.nix { inherit lib pkgs; }) mkConfigFile;
  bindings = [
    # "Mute" media key
    {
      keys = [ 113 ];
      events = [ "key" ];
      command = "${pkgs.wireplumber}/bin/wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle";
    }
    # "Lower Volume" media key
    {
      keys = [ 114 ];
      events = [
        "key"
        "rep"
      ];
      command = "${pkgs.graceful}/bin/graceful volume decrease";
    }
    # "Raise Volume" media key
    {
      keys = [ 115 ];
      events = [
        "key"
        "rep"
      ];
      command = "${pkgs.graceful}/bin/graceful volume increase";
    }
    # "Mic Mute" media key
    {
      keys = [ 190 ];
      events = [ "key" ];
      command = "${pkgs.wireplumber}/bin/wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle";
    }
  ];
in
{
  options = {
    enable = lib.mkEnableOption "global pipewire with actkbd keyboard shortcuts";
    user = lib.mkOption {
      default = null;
      type = with lib.types; nullOr str;
    };
  };

  config = lib.mkIf cfg.enable {
    # install useful related packages
    environment.systemPackages = with pkgs; [
      pavucontrol
      graceful
    ];

    # enable pipewire with everything and extra scharf
    security.rtkit.enable = true;
    services.pipewire = {
      systemWide = true;
      enable = true;
      audio.enable = true;
      wireplumber.enable = true;
      pulse.enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      # disable handsfree mode (trash audio quality)
      wireplumber.configPackages = [
        # from https://gitlab.freedesktop.org/pipewire/wireplumber/-/issues/634
        (pkgs.writeTextDir "share/wireplumber/wireplumber.conf.d/51-mitigate-annoying-profile-switch.conf" ''
          ## In WirePlumber there's a bug where some applications trigger switching to Headset Profile
          ## --
          ## See issue #634, #645, #630, #629, #613
          ## --
          ## This config mitigates the issue by completely disabling the switching and support for Headset Profile (HFP)
          ## Using this would only make sense if you never plan on using the microphone that comes with your headset.

          wireplumber.settings = {
            ## Whether to use headset profile in the presence of an input stream.
            ## --
            ## Disable for now, as it causes issues. See note at the top as to why.
            bluetooth.autoswitch-to-headset-profile = false
          }

          monitor.bluez.properties = {
            ## Enabled roles (default: [ a2dp_sink a2dp_source bap_sink bap_source hfp_hf hfp_ag ])
            ##
            ## Currently some headsets (Sony WH-1000XM3) are not working with
            ## both hsp_ag and hfp_ag enabled, so by default we enable only HFP.
            ##
            ## Supported roles: hsp_hs (HSP Headset),
            ##                  hsp_ag (HSP Audio Gateway),
            ##                  hfp_hf (HFP Hands-Free),
            ##                  hfp_ag (HFP Audio Gateway)
            ##                  a2dp_sink (A2DP Audio Sink)
            ##                  a2dp_source (A2DP Audio Source)
            ##                  bap_sink (LE Audio Basic Audio Profile Sink)
            ##                  bap_source (LE Audio Basic Audio Profile Source)
            ## --
            ## Only enable A2DP here and disable HFP. See note at the top as to why.
            bluez5.roles = [ a2dp_sink a2dp_source ]
          }

        '')
      ];
    };

    # set the selected user as the pipewire user and make sure it has the required groups
    systemd.services.pipewire.serviceConfig.User = lib.mkIf (cfg.user != null) (
      lib.mkOverride 90 cfg.user
    );
    systemd.services.wireplumber.serviceConfig.User = lib.mkIf (cfg.user != null) (
      lib.mkOverride 90 cfg.user
    );
    users.users = lib.mkIf (cfg.user != null) {
      ${cfg.user}.extraGroups = [
        "audio"
        "video"
        "pipewire"
      ] ++ lib.optional config.security.rtkit.enable "rtkit";
    };

    # keyboard shortcuts
    services.udev.packages = lib.singleton (
      pkgs.writeTextFile {
        name = "actkbd-volume-udev-rules";
        destination = "/etc/udev/rules.d/61-actkbd-volume.rules";
        text = lib.concatStringsSep ", " [
          ''ACTION=="add"''
          ''KERNEL=="event[0-9]*"''
          ''SUBSYSTEM=="input"''
          ''ENV{ID_INPUT_KEY}=="1"''
          ''ENV{ID_PATH}=="platform-i8042-serio-0"''
          ''TAG+="systemd"''
          ''ENV{SYSTEMD_WANTS}+="actkbd-volume@$env{DEVNAME}.service"''
        ];
      }
    );
    systemd.services."actkbd-volume@" = {
      restartIfChanged = true;
      unitConfig = {
        Description = "actkbd on %I for volume control";
        ConditionPathExists = "%I";
      };
      serviceConfig = {
        Type = "forking";
        ExecStart = "${pkgs.actkbd}/bin/actkbd -D -c ${mkConfigFile bindings} -d %I";

        # hardening

        ## allow access to event device
        DeviceAllow = [ "%I" ];
        DevicePolicy = "closed";

        ## these are apparantly specifically needed for wpctl
        CapabilityBoundingSet = [ "cap_dac_override" ];
        RestrictAddressFamilies = [ "AF_UNIX" ];

        IPAddressDeny = "any";
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        NoNewPrivileges = true;
        PrivateNetwork = true;
        PrivateTmp = true;
        ProtectClock = true;
        ProtectControlGroups = true;
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        ProtectProc = "invisible";
        ProtectSystem = "strict";
        RestrictNamespaces = true;
        RestrictRealtime = true;
        RestrictSUIDSGID = true;
        SocketBindDeny = "any";
        SystemCallArchitectures = "native";
        SystemCallFilter = "~@aio:EPERM @chown:EPERM @clock:EPERM @cpu-emulation:EPERM @debug:EPERM @keyring:EPERM @memlock:EPERM @module:EPERM @mount:EPERM @obsolete:EPERM @pkey:EPERM @privileged:EPERM @raw-io:EPERM @reboot:EPERM @resources:EPERM @sandbox:EPERM @setuid:EPERM @swap:EPERM @sync:EPERM @timer:EPERM";
        UMask = "0077";

        # -> 1.1 OK
      };
    };
  };
}
