{ lib, pkgs, ... }:
{
  mkConfigFile =
    bindings:
    bindings
    |> map (
      {
        keys,
        events ? [ "key" ],
        attributes ? [ "exec" ],
        command ? "",
        ...
      }:
      lib.concatStringsSep ":" [
        (lib.concatMapStringsSep "+" toString keys)
        (lib.concatStringsSep "," events)
        (lib.concatStringsSep "," attributes)
        command
      ]
    )
    |> lib.concatStringsSep "\n"
    |> pkgs.writeText "actkbd.conf";
}
