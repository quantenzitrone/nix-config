{
  lib,
  cfg,
  pkgs,
  ...
}:
let
  inherit (lib)
    all
    concatStrings
    elem
    fixedWidthString
    fromHexString
    genList
    id
    mkOption
    splitString
    stringLength
    substring
    toHexString
    toUpper
    types
    concatMapStrings
    ;

  unfoldColor =
    colorstr:
    let
      len = stringLength colorstr;
    in
    # make sure we have rrggbbaa or rrggbb
    assert elem len [
      3
      4
      6
      8
    ];
    let
      # parse RGB, RGBA RRGGBB and RRGGBBAA formats
      rgbaHex = toUpper (
        if len == 3 then
          (concatMapStrings (n: substring n 1 colorstr) (genList (x: x / 2) 6)) + "cc"
        else if len == 4 then
          concatMapStrings (n: substring n 1 colorstr) (genList (x: x / 2) 8)
        else if len == 6 then
          colorstr + "cc"
        else
          colorstr
      );
    in
    # make sure we have rrggbbaa form (8 chars)
    assert stringLength rgbaHex == 8;
    # make sure we only have hex characters
    assert
      splitString "" rgbaHex
      |> lib.filter (x: stringLength x != 0)
      |> all (x: elem x (genList id 16 |> map toHexString));
    rec {
      inherit rgbaHex;
      rgbHex = substring 0 6 rgbaHex;
      rHex = substring 0 2 rgbaHex;
      gHex = substring 2 2 rgbaHex;
      bHex = substring 4 2 rgbaHex;
      aHex = substring 6 2 rgbaHex;
      rInt = fromHexString rHex;
      gInt = fromHexString gHex;
      bInt = fromHexString bHex;
      aInt = fromHexString aHex;
      rFloat = rInt / 255.;
      gFloat = gInt / 255.;
      bFloat = bInt / 255.;
      aFloat = aInt / 255.;
    };

  mkColorOption =
    description: default:
    mkOption {
      type = types.strMatching "[0-9a-fA-F]{8}|[0-9a-fA-F]{6}|[0-9a-fA-F]{4}|[0-9a-fA-F]{3}";
      inherit default;
      description = ''
        ${description} in the form of RRGGBBAA or RRGGBB.
        Will automatically expand to integer and hex representations of all components.
      '';
      apply = unfoldColor;
    };

  dim =
    unfoldedColor:
    [
      unfoldedColor.rInt
      unfoldedColor.gInt
      unfoldedColor.bInt
    ]
    |> map (x: builtins.floor x - 20)
    |> map (
      x:
      if x > 255 then
        255
      else if x < 0 then
        0
      else
        x
    )
    |> map toHexString
    |> map (fixedWidthString 2 "0")
    |> concatStrings;

  brighten =
    unfoldedColor:
    [
      unfoldedColor.rInt
      unfoldedColor.gInt
      unfoldedColor.bInt
    ]
    |> map (x: builtins.floor x + 20)
    |> map (
      x:
      if x > 255 then
        255
      else if x < 0 then
        0
      else
        x
    )
    |> map toHexString
    |> map (fixedWidthString 2 "0")
    |> concatStrings;
in
{
  options = {
    themeType = mkOption {
      type = types.enum [
        "dark"
        "light"
      ];
      description = ''
        Type of theme: either dark or light.
        Determines if secondary colors get dimmed or brightened;
      '';
    };

    borderWidth = mkOption {
      type = types.ints.positive;
    };
    cornerRadius = mkOption {
      type = types.ints.positive;
    };
    gaps = mkOption {
      type = types.ints.positive;
    };

    icons = {
      package = lib.mkPackageOption pkgs "papirus" { };
      name = mkOption {
        type = lib.types.str;
      };
    };

    background = mkColorOption "Background color" "000000";
    background1 = mkColorOption "Secondary Background option" (
      (if cfg.themeType == "dark" then brighten else dim) cfg.background
    );
    foreground = mkColorOption "Foreground/text color" "ffffff";
    foreground1 = mkColorOption "Secondary foreground/text color" (
      (if cfg.themeType == "dark" then dim else brighten) cfg.foreground
    );
    accent = mkColorOption "Accent color" "aa00ff";
    termcolors = {
      normal = {
        black = mkColorOption "Terminal color black" "000000";
        white = mkColorOption "Terminal color white" "ffffff";
        red = mkColorOption "Terminal color red" "ff0000";
        yellow = mkColorOption "Terminal color yellow" "ffff00";
        green = mkColorOption "Terminal color green" "00ff00";
        cyan = mkColorOption "Terminal color cyan" "00ffff";
        blue = mkColorOption "Terminal color blue" "0000ff";
        magenta = mkColorOption "Terminal color magenta" "ff00ff";
      };
      bright = {
        black = mkColorOption "Terminal color bright black" (brighten cfg.termcolors.normal.black);
        white = mkColorOption "Terminal color bright white" (brighten cfg.termcolors.normal.white);
        red = mkColorOption "Terminal color bright red" (brighten cfg.termcolors.normal.red);
        yellow = mkColorOption "Terminal color bright yellow" (brighten cfg.termcolors.normal.yellow);
        green = mkColorOption "Terminal color bright green" (brighten cfg.termcolors.normal.green);
        cyan = mkColorOption "Terminal color bright cyan" (brighten cfg.termcolors.normal.cyan);
        blue = mkColorOption "Terminal color bright blue" (brighten cfg.termcolors.normal.blue);
        magenta = mkColorOption "Terminal color bright magenta" (brighten cfg.termcolors.normal.magenta);
      };
      dim = {
        black = mkColorOption "Terminal color bright black" (dim cfg.termcolors.normal.black);
        white = mkColorOption "Terminal color bright white" (dim cfg.termcolors.normal.white);
        red = mkColorOption "Terminal color bright red" (dim cfg.termcolors.normal.red);
        yellow = mkColorOption "Terminal color bright yellow" (dim cfg.termcolors.normal.yellow);
        green = mkColorOption "Terminal color bright green" (dim cfg.termcolors.normal.green);
        cyan = mkColorOption "Terminal color bright cyan" (dim cfg.termcolors.normal.cyan);
        blue = mkColorOption "Terminal color bright blue" (dim cfg.termcolors.normal.blue);
        magenta = mkColorOption "Terminal color bright magenta" (dim cfg.termcolors.normal.magenta);
      };
    };
  };
}
