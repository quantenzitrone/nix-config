{ lib, ... }:
let
  fontOption = name: {
    name = lib.mkOption {
      description = "name of the font";
      type = lib.types.str;
      default = name;
    };
    size = lib.mkOption {
      description = "size of the font";
      type = lib.types.int;
    };
  };
in
{
  options = {
    serif = fontOption "serif";
    sansSerif = fontOption "sans-serif";
    monospace = fontOption "monospace";
  };
}
