{
  cfg,
  lib,
  ...
}:
{
  options = {
    name = lib.mkOption {
      type = lib.types.str;
    };
    home = lib.mkOption {
      type = lib.types.str;
      default = "/home/${cfg.name}";
    };
  };
}
