{ lib, ... }:
{
  options.defaultFontSizes =
    lib.genAttrs
      [
        "serif"
        "sansSerif"
        "monospace"
      ]
      (
        name:
        lib.mkOption {
          type = lib.types.numbers.positive;
          description = "font size for ${name} text";
        }
      );
}
