# nixosModules

These modules are all in-one outputted to `nixosModules.default`.
I currently don't see a good way of having modularized modules.
To make them still reusable they are namespaced under `qz`.

## desktops

`config.qz.desktops.*` are fully configured desktop environments build from tiling window managers or wayland compositors. They mostly look and work the same, tailored to my preferences, but the theme is configurable through standardized options.

I try to use systemd as much as possible for starting related services, so each window manager has their own systemd target.
As of now (2024-01-10) i mostly use sway(fx), even though i3 looks better in some aspects (because picom shaders), on the other hand some other things like mouse window resizing and moving look better on sway.

## programs

`config.qz.programs.*` are preconfigured programs with their config files, maybe soon to be obsoleted by dotconfig.

## services

`config.qz.services.*` are system services themed and all. Stuff like login managers, mpd, idk.

## userservices

`config.qz.userservices.*` services that run in the userspace and are not specific to a desktop.

Some services from desktops can possibly be moved here with a configurable startOn argument like I did with xsettingsd and picom.

## variables

Not to be used, anymore, has been the previous way to configure everything conformly. Now I use the options as a template for all other services, from there I just inherit.
