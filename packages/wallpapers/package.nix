{ lib, fetchFromGitea }:
fetchFromGitea {
  domain = "codeberg.org";
  owner = "quantenzitrone";
  repo = "wallpapers";
  rev = "3a2602af7bb58e833ecfa6b098421d9d3af0284d";
  hash = "sha256-sHcJfiNYCfbRkH9VOUwiwCc0xnIvjD6EAWX5msHzcfI=";
}
// {
  meta = with lib; {
    description = ''
      Single storepath containing Quantenzitrone's wallpaper collection.
      Most images are not my intellectual property, hence this package is unfree.
    '';
    homepage = "https://codeberg.org/quantenzitrone/wallpapers";
    license = with licenses; [
      unfree
      cc-by-sa-40
    ];
    platforms = platforms.unix;
    maintainers = with maintainers; [ quantenzitrone ];
  };
}
