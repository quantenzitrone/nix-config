{ nautilus }:
nautilus.overrideAttrs (prevAttrs: {
  patches = prevAttrs.patches ++ [ ./0001-Initialized-English-Denmark-translation.patch ];
})
