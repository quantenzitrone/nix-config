{ streamrip, fetchFromGitHub }:
streamrip.overrideAttrs {
  version = "dev-2024-10-04";
  src = fetchFromGitHub {
    owner = "nathom";
    repo = "streamrip";
    rev = "46b570dbb6f81d604cbaa3bfa379463e0a20a841";
    hash = "sha256-LD99OjguaBnrQxCwmCeHvmBMq5aOfobwnMd5/aCRZW8=";
  };
}
