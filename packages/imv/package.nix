{ imv, fetchpatch }:
(imv.overrideAttrs {
  patches = [
    # add libnsgif 1.0.0 support
    # https://lists.sr.ht/~exec64/imv-devel/patches/49931
    (fetchpatch {
      url = "https://lists.sr.ht/~exec64/imv-devel/%3C20240301211856.8170-1-dev@kz6wk9.com%3E/raw";
      hash = "sha256-4sqEU6SqlYSlvCUpGYs86omIe0SW6J8xqCygiwlwBps=";
    })
    # add decoding bmp files
    # https://lists.sr.ht/~exec64/imv-devel/patches/50417
    (fetchpatch {
      url = "https://lists.sr.ht/~exec64/imv-devel/%3CDB6P189MB045422823D2F693CD4BEB843D1372@DB6P189MB0454.EURP189.PROD.OUTLOOK.COM%3E/raw";
      hash = "sha256-LlhxCQ6QmkAlSWt/lnUPywuXu39a2fHT00usR+3KVxE=";
    })
    # add svg+xml to mimetypes
    # https://lists.sr.ht/~exec64/imv-devel/patches/50446
    (fetchpatch {
      url = "https://lists.sr.ht/~exec64/imv-devel/%3C20240325200130.26778-1-johan@forberg.se%3E/raw";
      hash = "sha256-Ja7Cfm2sp87gf/g+He/nPxBrJq+BcPdmcFTk5qg9RsM=";
    })
    # support large images
    # https://lists.sr.ht/~exec64/imv-devel/patches/52942
    (fetchpatch {
      url = "https://lists.sr.ht/~exec64/imv-devel/%3C20240526014623.1273866-2-scott092707@aol.com%3E/raw";
      hash = "sha256-9sL3tFS6d0OmbL7ZDQv2yCXDuowclAuhVccn4XRcobo=";
    })
  ];
}).override
  {
    withBackends = [
      "libnsgif"
      "libjxl"
      "libtiff"
      "libjpeg"
      "libpng"
      "librsvg"
      "libheif"
    ];
  }
