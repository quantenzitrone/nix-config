# Packages and Overlays

This directory contains all packages and overlays.
Overlays are just packages that depend on the original package.
Instead of recursing infinitely, dependencies on itself are taken from the `prev` package set.
In this case the `prev` package set is nixpkgs and the `final` are the packages in this repo.

for explaination assume this simplified directory structure:

```tree
.
├── some-pkg
│   └── package.nix
├── ignored
│   └── ignored.nix
├── some-overlay
│   └── package.nix
├── overlays.nix
├── packages.nix
└── README.md
```

### every subfolder containing a `package.nix` gets autocalled by `./packages.nix` and `./overlays.nix`

```nix
{
    some-pkg = callPackage some-pkg/package.nix {};
    some-overlay = callPackage some-overlay/package.nix {};
}
```

and

```nix
{
    some-pkg = final: prev: {
        some-pkg = callPackage some-pkg/package.nix {};
    };
    some-overlay = final: prev: {
        some-overlay = callPackage some-overlay/package.nix {};
    };
    default = final: prev: {
        some-pkg = callPackage some-pkg/package.nix {};
        some-overlay = callPackage some-overlay/package.nix {};
    }
}
```
