final: prev: name:
let
  inherit (prev.lib) callPackageWith optionalAttrs hasAttr;
in
callPackageWith (
  # packages from nixpkgs
  prev
  # extra package sets i want to include
  // prev.xorg
  # packages from this flake
  // final
  # the called package from nixpkgs to prevent running into infinite recursion
  // optionalAttrs (hasAttr name prev) { ${name} = prev.${name}; }
)
