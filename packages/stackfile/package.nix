{
  lib,
  rustPlatform,
  fetchFromGitea,
}:
rustPlatform.buildRustPackage rec {
  pname = "stackfile";
  version = "0.0";
  src = fetchFromGitea {
    domain = "codeberg.org";
    owner = "quantenzitrone";
    repo = "stackfile";
    rev = "v${version}";
    hash = "sha256-1ASNWErCpVLO0MmyJTQGrzHThgXMxOZh8N8C/3jo7DY=";
  };
  useFetchCargoVendor = true;
  cargoHash = "sha256-axLmW391xGst4obn2w+GrUmxfFH2XH8k8WpjFjyhvcg=";
  meta = {
    description = "Use a file as memory for a stack (LIFO) of strings";
    homepage = "https://codeberg.org/quantenzitrone/stackfile";
    license = lib.licenses.gpl3Plus;
    platforms = lib.platforms.linux;
    maintainers = with lib.maintainers; [ quantenzitrone ];
  };
}
