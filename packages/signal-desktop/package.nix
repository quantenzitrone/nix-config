# wraps signal-desktop to use the tray icon
{
  signal-desktop,
  symlinkJoin,
  makeDesktopItem,
}:
symlinkJoin {
  inherit (signal-desktop) pname version;
  paths = [
    (makeDesktopItem {
      name = "signal-desktop";
      desktopName = "Signal";
      exec = "signal-desktop --use-tray-icon --ozone-platform-hint=auto --no-sandbox %U";
      terminal = false;
      type = "Application";
      icon = "signal-desktop";
      startupWMClass = "signal";
      comment = "Private messaging from your desktop";
      mimeTypes = [
        "x-scheme-handler/sgnl"
        "x-scheme-handler/signalcaptcha"
      ];
      categories = [
        "Network"
        "InstantMessaging"
        "Chat"
      ];
    })
    signal-desktop
  ];
}
