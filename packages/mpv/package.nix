{ mpv, mpvScripts }:
(mpv.overrideAttrs (prevAttrs: {
  postFixup = ''
    cp $out/share/applications/mpv.desktop{,2}
    mv $out/share/applications/mpv.desktop{2,}
    substituteInPlace $out/share/applications/mpv.desktop \
      --replace-fail "mpv --player-operation-mode=pseudo-gui" "$out/bin/mpv"
  '';
})).override
  { scripts = with mpvScripts; [ sponsorblock ]; }
