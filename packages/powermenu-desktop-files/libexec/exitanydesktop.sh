#!/bin/sh
if test -n "${WAYLAND_DISPLAY:-}"; then
	wayland-logout
elif test -n "${DISPLAY:-}"; then
	case "${XDG_CURRENT_DESKTOP:-unset}" in
	"unset") echo "XDG_CURRENT_DESKTOP is unset" ;;
	"none+i3" | "i3") i3 exit ;;
	# TODO find out how to exit from these desktops
	"Cinnamon" | "X-Cinnamon") cinnamon-session-quit --logout --force ;;
	#"Deepin" | "DEEPIN" | "deepin") no way fonud :( ;;
	# "ENLIGHTENMENT") no way found :( ;;
	#"GNOME") gnome-session-quit doesn't work for gnome 40+;;
	#"GNOME-Flashback");;
	"KDE") qdbus org.kde.ksmserver /KSMServer logout 0 0 0 ;;
	#"LXDE");;
	"LXQt") lxqt-leave --logout ;;
	"MATE") mate-session-save --logout ;;
	"XFCE") xfce4-session-logout --logout ;;
	#"Unity");;
	#"Pantheon");;
	#"TDE");;
	#"EDE");;
	"Budgie:GNOME") gnome-session-quit --logout --no-prompt ;;
	# TODO add more possible desktops here
	*) echo "Unsupported Desktop \"$XDG_CURRENT_DESKTOP\"" ;;
	esac
else
	echo "Neither DISPLAY nor WAYLAND_DISPLAY is set."
	echo "Is a graphical environment running?"
fi
