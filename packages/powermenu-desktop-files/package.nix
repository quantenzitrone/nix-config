{
  lib,
  stdenv,
  wayland-logout,
}:
stdenv.mkDerivation {
  name = "powermenu-desktop-files";
  src = lib.cleanSourceWith {
    filter = name: type: !(lib.hasSuffix ".nix" (baseNameOf (toString name)));
    src = ./.;
  };
  installPhase = ''
    cp -r $src $out
  '';
  fixupPhase = ''
    substituteInPlace $out/share/applications/logout.desktop\
      --replace "/usr/libexec" "$out/libexec"
    substituteInPlace $out/libexec/exitanydesktop.sh\
      --replace "wayland-logout" "${wayland-logout}/bin/wayland-logout"
  '';
  meta = with lib; {
    description = ''
      Provides .desktop files for all powermenu actions: shutdown, reboot, suspend, hibernate, logout, lock-screen.
      Logout only works for a small number of DMs/WMs (currently only i3);
    '';
    homepage = "https://codeberg.org/quantenzitrone/nix-config";
    license = with licenses; [ gpl3Plus ];
    platforms = platforms.linux;
    maintainers = with maintainers; [ quantenzitrone ];
  };
}
