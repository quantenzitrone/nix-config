#!/usr/bin/env bash

# Script toggles the enabled state of a xinput device with Touchpad in its name.
# Only really works on if there is only one device with this name, it will select the first one.
# Well it works if that first one is the one you need, otherwise change the grep search string.
#
# This scipt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This scipt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script.  If not, see <https://www.gnu.org/licenses/>.

num=$(xinput list | grep -m 1 Touchpad | sed 's/\s/\n/g' | grep id= | cut -c 4-)

if [ "$(xinput list "$num" | grep -o 'disabled')" = 'disabled' ]; then
	xinput enable "$num"
else
	xinput disable "$num"
fi
