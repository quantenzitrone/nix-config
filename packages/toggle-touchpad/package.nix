{
  lib,
  writeShellApplication,
  xinput,
}:
writeShellApplication {
  name = "toggle-touchpad";
  runtimeInputs = [
    xinput
  ];
  text = builtins.readFile ./toggle-touchpad.sh;
}
// {
  meta = with lib; {
    description = ''
      A script to dis- and enable the touchpad on your laptop.
      Only tested on one device yet so...
    '';
    homepage = "https://codeberg.org/quantenzitrone/nix-config";
    license = with licenses; [ gpl3Plus ];
    platforms = platforms.linux;
    maintainers = with maintainers; [ quantenzitrone ];
  };
}
