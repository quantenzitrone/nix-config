# autocall all packages as overlays
{ lib, ... }:
let
  inherit (lib) filterAttrs pathIsRegularFile mapAttrs;

  packages = prev: import ./packages.nix { pkgs = prev; };

  overlays =
    builtins.readDir ./.
    |> filterAttrs (name: _: pathIsRegularFile ./${name}/package.nix)
    |> mapAttrs (name: _: final: prev: { ${name} = (packages prev).${name}; });

  allOverlays = final: prev: packages prev;
in
overlays // { default = allOverlays; }
