# wraps mpc for mpc playlist to format nicely
{
  mpc-cli,
  writeShellApplication,
  symlinkJoin,
  ...
}:
symlinkJoin {
  name = "mpc-cli-wrapped";
  paths = [
    (writeShellApplication {
      name = "mpc";
      runtimeInputs = [ mpc-cli ];
      text = ''
        if [ "''${1:-""}" = "playlist" ]; then
        	mpc playlist -f "%position% %artist% - %title%"
        else
        	mpc "$@"
        fi
      '';
    })
    mpc-cli
  ];
}
// {
  meta = mpc-cli.meta;
}
