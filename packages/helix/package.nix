{ helix, fetchpatch2 }:
helix.overrideAttrs (prevAttrs: {
  patches = prevAttrs.patches ++ [
    ## mouse support
    # bufferline
    (fetchpatch2 {
      url = "https://patch-diff.githubusercontent.com/raw/helix-editor/helix/pull/12173.patch";
      hash = "sha256-ETFyZ4ECZxBSJWopHqYZUtU59jVT0yn03bhxF+jet70=";
    })
    # editor double and triple click
    (fetchpatch2 {
      url = "https://patch-diff.githubusercontent.com/raw/helix-editor/helix/pull/12514.patch";
      hash = "sha256-gnTCGjr1OOCMb7LtXRpZmzT0ZFNs136B4X76bxx5sVc=";
    })
    # picker
    (fetchpatch2 {
      url = "https://patch-diff.githubusercontent.com/raw/helix-editor/helix/pull/12772.patch";
      hash = "sha256-rtGjTk4bXQxYoNcAyqGIR37ukxL+3YHwV7EM25FVbP0=";
    })
  ];
})
