{
  lib,
  writeScriptBin,
  fish,
  coreutils-full,
  gnused,
  ncurses,
  playerctl,
  ...
}:
writeScriptBin "ufetch" (
  ''
    #!${fish}/bin/fish
    export PATH="${
      builtins.concatStringsSep ":" (
        map (x: "${x}/bin") [
          coreutils-full
          gnused
          ncurses
          playerctl
        ]
      )
    }:$PATH"
  ''
  + (builtins.readFile ./ufetch.fish)
)
// {
  meta = with lib; {
    description = ''
      Small fetch script to get some system info + nixos ascii art.
    '';
    homepage = "https://codeberg.org/quantenzitrone/nix-config";
    license = with licenses; [ gpl3Plus ];
    platforms = platforms.linux;
    maintainers = with maintainers; [ quantenzitrone ];
  };
}
