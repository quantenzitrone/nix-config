#!/usr/bin/env fish

# define colors
################################################
set bold "$(tput bold)"
set black "$(tput setaf 0)"
set red "$(tput setaf 1)"
set green "$(tput setaf 2)"
set yellow "$(tput setaf 3)"
set blue "$(tput setaf 4)"
set magenta "$(tput setaf 5)"
set cyan "$(tput setaf 6)"
set white "$(tput setaf 7)"
set reset "$(tput sgr0)"

# useful non-overridable function
function revcut
    rev | cut $argv | rev
end

# parse help
################################################
argparse h/help -- $argv
if set -q _flag_help
    set bb $reset$blue$bold
    echo \
        $bb"NAME$reset
    ufetch - small fetcher script (NixOS version)$bb
SYNOPSIS$reset$green
    ufetch$reset [--help]$bb
CONFIGURATION FILES$reset
    \$XDG_CONFIG_HOME/ufetch/nixos.fish
    \$HOME/.config/ufetch/nixos.fish
    /etc/xdg/ufetch/nixos.fish$bb
CONFIGURATION SYNTAX$reset$green
    set$reset <variable> <value>$green
    function$reset <function>
        <fish code to do the stuff>$green
    end$bb
CONFIGURATION OPTIONS$reset
    every function and variable defined before the config file import can be overridden
    the code is well commented and not too long so i don't think more information is needed here
"
    exit 0
end

# define overrideable functions and variables
################################################
# the ascii art to print override for custom ascii art
function art
    set a "$reset$argv[1]"
    set b "$reset$argv[2]"
    set c "$reset$argv[3]"
    set d "$reset$argv[4]"
    set e "$reset$argv[5]"
    set f "$reset$argv[6]"
    set art \
        "$a      ▗▄▄     $b▗▄▄   ▄▄▖     " \
        "$a       ▜█▙     $b▜█▙ ▟█▛      " \
        "$a   ▄▄▄▄▄██▙▄▄▄▄ $b▜███▛       " \
        "$a  ▝▀▀▀▀▀▀▀▀▀▀▀▀▘ $b▜██   $c▟▙   " \
        "$f       ▟█▛        $b▜█▙ $c▟█▛   " \
        "$f      ▟█▛          $b▜▘$c▟█▛    " \
        "$f▜██████▛            $c▟██████▙" \
        "$f    ▟█▛$e▗▙          $c▟█▛      " \
        "$f   ▟█▛ $e▜█▙        $c▟█▛       " \
        "$f   ▜▛  $e ██▙ $d▗▄▄▄▄▄▄▄▄▄▄▄▄▖  " \
        "$e       ▟███▙ $d▀▀▀▀▜██▀▀▀▀▀   " \
        "$e      ▟█▛ ▜█▙     $d▜█▙       " \
        "$e     ▝▀▀   ▀▀▘     $d▀▀▘      "
    echo (string join "<split>" $art)
end
# to get a list of file systems
# change this to `echo <mountpoint1> <mountpoint2> ...` for custom selection
function fs_list
    df --output=source,target \
        | grep '^/dev' \
        | grep -v -e loop -e boot \
        | awk '{ printf $2 " "}' | awk '$1=$1' # formatting
end
# to get file system fullness
function get_fs
    df --output=used,size,pcent $argv[1] | tail -n+2 | awk '{print int($i/1048576) "/" int($2/1048576) "GiB (" $3 ")"}'
end
# to parse uptime into a custom format
function parse_uptime
    set secs $argv[1]
    set seconds (math $secs % 60)
    set minutes (math floor $secs / 60 % 60)
    set hours (math floor $secs / 3600 % 24)
    set days (math floor $secs / 86400 % 365)
    set years (math floor $days / 31536000)
    if test $years -gt 0
        echo -n $years "years "
    end
    if test $days -gt 0
        echo -n $days "days "
    end
    if test $hours -gt 0
        echo -sn $hours "h "
    end
    if test $minutes -gt 0
        echo -sn $minutes "m "
    end
    echo -sn $seconds s
end
# to combine the ascii art and system info
function combine
    set art (string split "<split>" $argv[1])
    set info (string split "<split>" $argv[2])
    if test (count $art) -gt (count $info)
        set len (count $art)
    else
        set len (count $info)
    end
    for i in (seq $len)
        echo "$art[$i] $info[$i]"
    end
end
# the song to print (can be overridden to use a different method or format)
set song "$(playerctl metadata artist 2> /dev/null) - $(playerctl metadata title 2> /dev/null)"
# return uptime in seconds
set uptime (math (date +%s) - (stat -c %Z /proc/))
# labels color and format
set labelcolor "$bold$blue"
# info color
set infocolor "" # normal text by default
# colors of the flake
set color1 $blue
set color2 $cyan
set color3 $blue
set color4 $cyan
set color5 $blue
set color6 $cyan
# separator between label and info
set separator " - "
# set information attributes
set nixosversion NixOS (nixos-version)
set linuxversion Linux (uname -r)
set desktopinfo DE/WM (if set -q XDG_CURRENT_DESKTOP; echo $XDG_CURRENT_DESKTOP; else; echo none; end)
set terminfo Term $TERM
set shellinfo Shell (basename $SHELL)

# parse config file
################################################
# look which file to use and source it
if test -e $XDG_CONFIG_HOME/ufetch/nixos.fish -a (set -q $XDG_CONFIG_HOME)
    source "$XDG_CONFIG_HOME/ufetch/nixos.fish"
else if test -e $HOME/.config/ufetch/nixos.fish
    source "$HOME/.config/ufetch/nixos.fish"
else if test -e /etc/xdg/ufetch/nixos.fish
    source "/etc/xdg/ufetch/nixos.fish"
    # else none
end

# setting the info
################################################
set lc "$reset$labelcolor"
set ic "$reset$infocolor"
function colorize -S
    echo "$lc$(printf '%8s' $argv[1])$separator$ic$argv[2]"
end
set -a info (colorize $nixosversion)
set -a info (colorize $linuxversion)
set -a info (colorize "Uptime"    "$(parse_uptime $uptime)")
set -a info (colorize $desktopinfo)
set -a info (colorize $terminfo)
set -a info (colorize $shellinfo)

# get info of all filesystems
set fs_list (string split " " (fs_list))
for i in (seq (count $fs_list))
    set -a info (colorize $fs_list[$i] (get_fs $fs_list[$i]))
end

set -a info (colorize "Song" "$(if test "$(echo $song | cut -c -36)" = "$song"; echo "$song"; else; echo "$(echo $song | cut -c -33)..."; end)")
set -a info ""
set -a info \
    "  $reset$black███   $reset$red███   $reset$yellow███\
   $reset$green███   $reset$cyan███   $reset$blue███\
   $reset$magenta███   $reset$white███"

## OUTPUT
combine (art $red $yellow $green $cyan $blue $magenta) (string join "<split>" $info)
