#!/usr/bin/env bash

case "${1:-}" in
vol | volume)
	current=$(bc <<<"$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{print $2}')*100/1")

	case "${2:-}" in
	d | decrease)
		if [ "$current" -le 10 ]; then
			wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ "1%-"
		else
			wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ "$((current / 5 * 5 - 5))%"
		fi
		;;
	i | increase)
		if [ "$current" -lt 10 ]; then
			wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ "1%+"
		else
			wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ "$((current / 5 * 5 + 5))%"
		fi
		;;
	esac
	;;
bri | bright | brightness)
	current=$((100 * $(brightnessctl get) / $(brightnessctl max)))

	case "${2:-}" in
	d | decrease)
		if [ "$current" -le 10 ]; then
			brightnessctl set "1%-"
		else
			brightnessctl set "$((current / 5 * 5 - 5))%"
		fi
		;;
	i | increase)
		if [ "$current" -lt 10 ]; then
			brightnessctl set "+1%"
		else
			brightnessctl set "$((current / 5 * 5 + 5))%"
		fi
		;;
	esac
	;;
esac
