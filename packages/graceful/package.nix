{
  writeShellApplication,
  wireplumber,
  brightnessctl,
  bc,
  gawk,
  ...
}:
writeShellApplication {
  name = "graceful";
  runtimeInputs = [
    wireplumber
    brightnessctl
    bc
    gawk
  ];
  text = builtins.readFile ./graceful.sh;
}
