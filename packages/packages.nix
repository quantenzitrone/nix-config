# autocall all packages
{ pkgs, ... }:
let
  inherit (pkgs.lib) filterAttrs pathIsRegularFile mapAttrs;

  callPackage = import ./callPackage.nix final pkgs;

  final =
    builtins.readDir ./.
    |> filterAttrs (name: _: pathIsRegularFile ./${name}/package.nix)
    |> mapAttrs (name: _: callPackage name ./${name}/package.nix { });
in
final
