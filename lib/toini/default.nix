{ lib, ... }:
{
  truestr ? "true",
  falsestr ? "false",
  quotestr ? "",
  prelist ? "",
  listsep ? " ",
  postlist ? "",
}:
(lib.generators.toINI {
  mkKeyValue =
    let
      mkValue =
        value:
        (
          if builtins.isBool value then
            (if value then truestr else falsestr)
          else if builtins.isString value then
            quotestr + value + quotestr
          else if builtins.isInt value then
            builtins.toString value
          else if builtins.isFloat value then
            builtins.toString value
          else if builtins.isList value then
            (prelist + builtins.concatStringsSep listsep (builtins.map mkValue value) + postlist)
          else
            abort "unhandled value type ${builtins.typeOf value}"
        );
    in
    key: value: key + "=" + mkValue value;
})
