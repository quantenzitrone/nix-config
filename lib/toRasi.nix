{ lib }:
let
  inherit (lib)
    concatStringsSep
    isList
    ;

  inherit (lib.generators)
    mkValueStringDefault
    toKeyValue
    ;

  mkRasiValue =
    value:
    if isList value then
      "[${concatStringsSep "," (map mkRasiValue value)}]"
    else
      mkValueStringDefault { } value;

  toRasi = toKeyValue {
    mkKeyValue = key: value: ''
      ${key} {
        ${
          toKeyValue {
            indent = "\t";
            mkKeyValue = key: value: "${key}: ${mkRasiValue value};";
          } value
        }}
    '';
  };

  quote = x: ''"${x}"'';
in
{
  inherit quote toRasi;
}
