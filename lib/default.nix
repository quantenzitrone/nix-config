{ lib, ... }:
{
  xdg = builtins.mapAttrs (name: _: import ./xdg/${name}) (builtins.readDir ./xdg);
  inherit (import ./toRasi.nix { inherit lib; }) toRasi quote;
  caddy = import ./caddy.nix;
}
