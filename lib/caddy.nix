{
  httpCat = ''
    handle_errors {
      rewrite * /{http.error.status_code}
      reverse_proxy https://http.cat {
        header_up Host http.cat
      }
    }
  '';

  errorGit = ''
    error /.git* 403
  '';
}
