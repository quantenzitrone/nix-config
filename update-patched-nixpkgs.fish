#!/usr/bin/env fish

# cd into the worktree
cd nixpkgs || exit 1

# only run in a worktree
git status >/dev/null || exit 1

# update to newest nixpkgs upstream
git fetch upstream
git reset --hard upstream/nixos-unstable

# cherry pick all needed commits
set pulls \
    # fish 4.0
    01c84b363718c9d06ce913fb09133f656443e4b4~..adf609a8d710b156922b0e626eaccd7e38b60bfe \
    # brightnessctl
    actkbd-brightness \
    # mbzero
    mbzero \
    # rss-bridge
    stunkymonkey/rss-bridge-webserver~5..stunkymonkey/rss-bridge-webserver \
    # searxng
    3c2e5ab61195c31d586f404f0224c2c8c9ab0244 \
    a4aadaa4743cac2b6f3e19aa0d7ea3b00622d8c1

for pull in $pulls
    git cherry-pick $pull
end

# update the flake.lock
cd ..
nix flake update nixpkgs
