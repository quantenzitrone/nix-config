{ lib, ... }:
let
  inherit (builtins) readDir;
  inherit (lib)
    concatStringsSep
    filterAttrs
    filterAttrsRecursive
    flatten
    getAttrFromPath
    isAttrs
    listToAttrs
    mapAttrs
    mapAttrsToList
    nameValuePair
    optional
    optionalAttrs
    pathExists
    setAttrByPath
    ;

  recursiveReadDir =
    dir:
    readDir dir
    |> mapAttrs (
      n: v:
      if v == "directory" || (v == "symlink" && pathExists "${toString /${dir}/${n}}/") then
        recursiveReadDir /${dir}/${n}
      else
        v
    );

  filterDirs =
    dirs:
    filterAttrsRecursive (
      n: v:
      if isAttrs v && n == "module.nix" then
        builtins.throw "you cannot have a module called module.nix"
      else
        isAttrs v || n == "module.nix"
    ) dirs;

  mkMappedModule =
    path:
    { pkgs, config, ... }@args:
    let
      modulepath = "${concatStringsSep "/" path}/module.nix";
      module = import ./${modulepath} (
        args
        // {
          cfg = getAttrFromPath path config;
        }
      );
      nullmodule = {
        options = null;
        config = null;
      };
    in
    if (module // nullmodule) != nullmodule then
      abort "module ${modulepath} has other attributes than options & config"
    else
      {
        options = optionalAttrs (module ? options) (setAttrByPath path module.options);
        config = optionalAttrs (module ? config) module.config;
      };

  removeImports = x: removeAttrs x [ "imports" ];

  mkModuleRecursive =
    path: attrs:
    if attrs ? _module then
      throw "you cannot have a module called _module"
    else if attrs == { "module.nix" = "regular"; } then
      { _module = mkMappedModule path; }
    else
      let
        filteredAttrs = filterAttrs (n: _: n != "module.nix") attrs;
        recMappedAttrs = mapAttrs (n: v: mkModuleRecursive (path ++ [ n ]) v) filteredAttrs;
      in
      recMappedAttrs
      // {
        _module = {
          imports =
            (
              recMappedAttrs
              |> mapAttrsToList (
                _: v:
                if v._module ? imports then
                  v._module.imports ++ lib.optional (removeImports v._module != { }) (removeImports v._module)
                else
                  v._module
              )
              |> flatten
            )
            ++ optional (attrs ? "module.nix") (mkMappedModule path);
        };
      };

  flattenModules =
    path: modules:
    mapAttrsToList (
      n: v:
      if n == "_module" && path == [ ] then
        (nameValuePair "default" v)
      else if n == "_module" then
        (nameValuePair (concatStringsSep "." path) v)
      else
        (flattenModules (path ++ [ n ]) v)
    ) modules;
in
recursiveReadDir ./.
|> filterDirs
|> filterAttrsRecursive (_: v: v != { })
|> mkModuleRecursive [ ]
|> flattenModules [ ]
|> flatten
|> listToAttrs
