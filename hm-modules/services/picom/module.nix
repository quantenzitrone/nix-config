{
  cfg,
  lib,
  ...
}:
{
  options.startOn = lib.mkOption {
    description = "units to start picom on";
    type = with lib.types; listOf str;
    default = [ ];
  };

  config = lib.mkIf cfg.enable {
    systemd.user.services.picom = {
      Unit.PartOf = lib.mkOverride 90 cfg.startOn;
      Install.WantedBy = lib.mkOverride 90 cfg.startOn;
    };
  };
}
