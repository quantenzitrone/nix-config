{
  cfg,
  config,
  lib,
  ...
}:
{
  options.enable = lib.mkEnableOption "zitrone htop config";
  config = lib.mkIf cfg.enable {
    programs.htop = {
      enable = true;
      settings =
        {
          # ui
          color_scheme = 0;
          enable_mouse = 1;

          # main ui
          fields = with config.lib.htop.fields; [
            PID
            USER
            PRIORITY
            NICE
            M_SIZE
            M_RESIDENT
            M_SHARE
            STATE
            PERCENT_CPU
            PERCENT_MEM
            TIME
            COMM
          ];
          hide_kernel_threads = 1;
          hide_userland_threads = 1;
          hide_threads = 1;
          shadow_other_users = 1;
          show_thread_names = 0;
          show_program_path = 0;
          highlight_base_name = 1;

          # top ui
          header_layout = "two_67_33";
          show_cpu_usage = 1;
          show_cpu_frequency = 0;
          show_cpu_temperature = 1;
        }
        // (
          with config.lib.htop;
          leftMeters [
            (bar "AllCPUs")
            (bar "Memory")
            (bar "Swap")
          ]
        )
        // (
          with config.lib.htop;
          rightMeters [
            (text "Tasks")
            (text "LoadAverage")
            (text "Uptime")
            (text "Systemd")
            (text "Battery")
            (text "DiskIO")
          ]
        );
    };
  };
}
