{
  cfg,
  config,
  lib,
  ...
}:
let
  inherit (config.definitions.theme) foreground accent;
in
{
  options = {
    enable = lib.mkEnableOption "flameshot with theming";
    savePath = lib.mkOption { type = lib.types.str; };
  };

  config = lib.mkIf cfg.enable {
    services.flameshot = {
      enable = true;
      settings = {
        General = {
          inherit (cfg) savePath;
          filenamePattern = "screenshot-%FT%H%M%T%z";
          savePathFixed = true;
          # colors
          contrastUiColor = "#${foreground.rgbHex}";
          uiColor = "#${accent.rgbHex}";
          # hide everything
          disabledGrimWarning = true;
          disabledTrayIcon = true;
          showDesktopNotification = false;
          showHelp = false;
          showSidePanelButton = false;
          showStartupLaunchMessage = false;
        };
      };
    };

    systemd.user.services.flameshot = {
      Install = lib.mkOverride 90 { };
    };
  };
}
