{ pkgs }:
let
  nixIcon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";

  wikipediaIcon = pkgs.fetchurl {
    url = "https://upload.wikimedia.org/wikipedia/commons/8/80/Wikipedia-logo-v2.svg";
    hash = "sha256-kOAe969TprYk+iTuOkjH1t2fVbD5mbyMcwP40ons15g=";
  };

  wiktionaryIcon = pkgs.fetchurl {
    url = "https://upload.wikimedia.org/wikipedia/commons/e/ec/Wiktionary-logo.svg";
    hash = "sha256-MCSL5ALFiQwDQLgonm9aSCtM2sGvf9GGWytWwz3xSr0=";
  };
in
{
  # my searxng instance
  "SearXNG" = {
    urls = [ { template = "https://search.quantenzitrone.eu/search?q={searchTerms}"; } ];
    icon = pkgs.fetchurl {
      url = "https://raw.githubusercontent.com/searxng/searxng/7c80807bb8204e9ef0df9a39207e30587c856c0a/searx/static/themes/simple/img/favicon.svg";
      hash = "sha256-yFL5EtaVUOS6FqLIfrfMCB1jvPmm2dJao7UiNWHeQo0=";
    };
    definedAliases = [
      "@sx"
      "@searxng"
    ];
  };

  # nixos search
  "Nix Packages" = {
    urls = [ { template = "https://search.nixos.org/packages?channel=unstable&query={searchTerms}"; } ];
    icon = nixIcon;
    definedAliases = [
      "@np"
      "@nixpkgs"
    ];
  };
  "NixOS Options" = {
    urls = [ { template = "https://search.nixos.org/options?channel=unstable&query={searchTerms}"; } ];
    icon = nixIcon;
    definedAliases = [
      "@no"
      "@nixopts"
    ];
  };
  "Noogle" = {
    urls = [ { template = "https://noogle.dev/q?term={searchTerms}"; } ];
    icon = nixIcon;
    definedAliases = [
      "@nf"
      "@nixfuncs"
      "@noogle"
    ];
  };
  "NixOS Wiki" = {
    urls = [ { template = "https://wiki.nixos.org/w/index.php?search={searchTerms}"; } ];
    icon = nixIcon;
    definedAliases = [
      "@nw"
      "@nixoswiki"
    ];
  };

  # Wikipedia and Friends in German and English
  # zero width space at the end to not conflict with the builtin one
  "Wikipedia (en)​" = {
    urls = [ { template = "https://en.wikipedia.org/w/index.php?search={searchTerms}"; } ];
    icon = wikipediaIcon;
    definedAliases = [
      "@wpen"
      "@wikipediaen"
    ];
  };
  "Wikipedia (de)" = {
    urls = [ { template = "https://de.wikipedia.org/w/index.php?search={searchTerms}"; } ];
    icon = wikipediaIcon;
    definedAliases = [
      "@wpde"
      "@wikipediade"
    ];
  };
  "Wiktionary (en)" = {
    urls = [ { template = "https://en.wiktionary.org/w/index.php?search={searchTerms}"; } ];
    icon = wiktionaryIcon;
    definedAliases = [
      "@wten"
      "@wiktionaryen"
    ];
  };
  "Wiktionary (de)" = {
    urls = [ { template = "https://de.wiktionary.org/w/index.php?search={searchTerms}"; } ];
    icon = wiktionaryIcon;
    definedAliases = [
      "@wtde"
      "@wiktionaryde"
    ];
  };
  "Wikimedia Commons" = {
    urls = [ { template = "https://commons.wikimedia.org/w/index.php?search={searchTerms}"; } ];
    icon = pkgs.fetchurl {
      url = "https://upload.wikimedia.org/wikipedia/commons/4/4a/Commons-logo.svg";
      hash = "sha256-I3oWo7cA3zTKDx7CBIklDaI2orcJ5Kr+CY133bTA80s=";
    };
    definedAliases = [
      "@wc"
      "@wikimediacommons"
    ];
  };

  # hide all trash buildin engines
  "Google".metaData.hidden = true;
  "Amazon.de".metaData.hidden = true;
  "Bing".metaData.hidden = true;
  # not trash but i have replaced it with my own
  "Wikipedia (en)".metaData.hidden = true;
}
