{ config, inputs }:
let
  inherit (inputs.self.lib) toRasi quote;
in
toRasi {
  configuration = {
    disable-history = false;
    display-drun = quote "🔎";
    display-run = quote "🔍";
    drun-display-format = quote "{name}";
    font = quote "sans-serif";
    icon-theme = quote "${config.definitions.theme.icons.name}";
    show-icons = true;
    sidebar-mode = false;
  };
}
+ ''
  @theme "theme.rasi"
''
