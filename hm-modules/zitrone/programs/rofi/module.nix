{
  cfg,
  config,
  inputs,
  lib,
  pkgs,
  ...
}:
{
  options.enable = lib.mkEnableOption "zitrones rofi config";

  config = lib.mkIf cfg.enable {
    home.packages = [ pkgs.rofi-wayland ];
    xdg.configFile = {
      "rofi/config.rasi".text = import ./config.rasi.nix { inherit config inputs; };
      "rofi/theme.rasi".text = import ./theme.rasi.nix { inherit config inputs; };
    };
  };
}
