{ config, inputs }:
let
  inherit (builtins) ceil;
  inherit (inputs.self.lib) toRasi quote;
  inherit (config.definitions) theme;
in

toRasi {
  "*" = {
    background-color = "#00000000";
    blink = true;
    border-color = "#${theme.accent.rgbaHex}";
    border-radius = theme.cornerRadius;
    cursor = "inherit";
    placeholder = quote "Search Applications";
    placeholder-color = "#${theme.foreground1.rgbaHex}";
    text-color = "#${theme.foreground.rgbaHex}";
    transparency = quote "real";
  };
  element = {
    background-color = "inherit";
    children = [
      "element-icon"
      "element-text"
    ];
    cursor = "pointer";
    orientation = "vertical";
    padding = "20 0";
  };
  element-icon = {
    size = 64;
  };
  element-text = {
    horizontal-align = 0.5;
  };
  "element.selected" = {
    border = ceil (theme.borderWidth / 2.0);
  };
  entry = {
    cursor = "text";
  };
  inputbar = {
    border = ceil (theme.borderWidth / 2.0);
    children = [
      "prompt"
      "entry"
    ];
    padding = 10;
    spacing = 10;
  };
  listview = {
    background-color = "inherit";
    children = [
      "element"
      "scrollbar"
    ];
    columns = 4;
    cycle = false;
    dynamic = true;
    flow = "horizontal";
    layout = "vertical";
    lines = 2;
    scrollbar = true;
  };
  mainbox = {
    children = [
      "inputbar"
      "listview"
    ];
    padding = 10;
    spacing = 10;
  };
  prompt = {
    enabled = true;
  };
  scrollbar = {
    handle-color = "#${theme.accent.rgbaHex}";
    handle-with = 10;
  };
  window = {
    background-color = "#${theme.background.rgbaHex}";
    border = theme.borderWidth;
    width = "35%";
  };
}
