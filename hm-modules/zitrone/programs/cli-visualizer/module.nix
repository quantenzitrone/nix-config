{
  cfg,
  lib,
  pkgs,
  ...
}:
{
  options.enable = lib.mkEnableOption "cli-visualizer with config";

  config = lib.mkIf cfg.enable {
    home.packages = [ pkgs.cli-visualizer ];
    xdg.configFile."vis/config".source = ./config.conf;
  };
}
