{ lib, config }:
let
  inherit (lib)
    nameValuePair
    mapAttrs'
    toLower
    ;

  colors = config.definitions.theme.termcolors.normal;
in
lib.generators.toINI { } {
  info.about = "Xournalpp custom color names";
  custom = mapAttrs' (name: value: nameValuePair (toLower value.rgbHex) name) colors;
}
