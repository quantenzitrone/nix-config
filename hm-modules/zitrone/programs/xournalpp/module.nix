{
  cfg,
  config,
  lib,
  pkgs,
  ...
}:
{
  options.enable = lib.mkEnableOption "zitrones xournalpp config";

  config = lib.mkIf cfg.enable {
    home.packages = [ pkgs.xournalpp ];

    xdg.configFile."xournalpp/toolbar.ini".text = import ./toolbar.ini.nix {
      inherit lib config;
    };

    xdg.configFile."xournalpp/colornames.ini".text = import ./colornames.ini.nix {
      inherit lib config;
    };
  };
}
