{ lib, config }:
let
  inherit (lib)
    concatStringsSep
    flatten
    toLower
    attrValues
    ;

  colors = config.definitions.theme.termcolors.normal;
in
lib.generators.toINI { } {
  "Cool Config" = {
    name = "Cool Config";
    toolbarLeft1 = concatStringsSep "," (flatten [
      "VERY_FINE"
      "FINE"
      "MEDIUM"
      "THICK"
      "VERY_THICK"
      "SEPARATOR"
      "DRAW_ELLIPSE"
      "DRAW_RECTANGLE"
      "DRAW_ARROW"
      "RULER"
      "DRAW_COORDINATE_SYSTEM"
      "DRAW_SPLINE"
      "SHAPE_RECOGNIZER"
      "TOOL_FILL"
      "SEPARATOR"
      "ROTATION_SNAPPING"
      "GRID_SNAPPING"
      "SEPARATOR"
      "COLOR_SELECT"
      (map (x: "COLOR(0x${toLower x.rgbHex})") (attrValues colors))
    ]);
    toolbarBottom2 = concatStringsSep "," [
      "OPEN"
      "NEW"
      "SAVE"
      "UNDO"
      "REDO"
      "COPY"
      "PASTE"
      "SPACER"
      "HAND"
      "HIGHLIGHTER"
      "ERASER"
      "PEN"
      "TEXT"
      "SELECT_OBJECT"
      "SELECT_RECTANGLE"
      "SELECT_REGION"
      "VERTICAL_SPACE"
      "SPACER"
      "GOTO_BACK"
      "GOTO_NEXT"
      "DELETE_CURRENT_PAGE"
      "INSERT_NEW_PAGE"
      "PRESENTATION_MODE"
      "ZOOM_FIT"
      "ZOOM_OUT"
      "ZOOM_IN"
      "FULLSCREEN"
    ];
  };
}
