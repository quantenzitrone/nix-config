{
  normal = {
    C-S-tab = "goto_previous_buffer";
    C-tab = "goto_next_buffer";
    C-w = ":buffer-close";
    end = "goto_line_end_newline";
  };
}
