{ theme }:
let
  transparent = "default";
in
{
  inherits = "base16_default";
  "ui.bufferline" = {
    bg = transparent;
    fg = "#${theme.foreground1.rgbHex}";
  };
  "ui.bufferline.active" = {
    fg = "#${theme.foreground.rgbHex}";
    bg = "#${theme.background1.rgbHex}";
  };
  "ui.gutter" = {
    bg = transparent;
    fg = "#${theme.foreground1.rgbHex}";
  };
  "ui.gutter.selected" = {
    fg = "#${theme.foreground.rgbHex}";
    bg = "#${theme.background1.rgbHex}";
  };
  "ui.help" = {
    bg = "#${theme.background1.rgbHex}";
  };
  "ui.linenr" = {
    bg = transparent;
    fg = "#${theme.foreground1.rgbHex}";
  };
  "ui.linenr.selected" = {
    fg = "#${theme.foreground.rgbHex}";
    bg = "#${theme.background1.rgbHex}";
  };
  "ui.menu" = {
    bg = "#${theme.background1.rgbHex}";
    fg = "#${theme.foreground1.rgbHex}";
  };
  "ui.menu.selected" = {
    fg = "#${theme.foreground.rgbHex}";
    bg = "#${theme.background1.rgbHex}";
  };
  "ui.popup" = {
    bg = "#${theme.background1.rgbHex}";
  };
  "ui.statusline" = {
    bg = "#${theme.background1.rgbHex}";
  };
}
