{
  cfg,
  config,
  lib,
  pkgs,
  ...
}:
{
  options = {
    enable = lib.mkEnableOption "zitrones helix config";
    minimal = lib.mkEnableOption "less language servers";
  };

  config = lib.mkIf cfg.enable {

    programs.helix = {
      enable = true;
      settings = {
        theme = "base16_default_modded";

        editor = {
          bufferline = "multiple";
          cursor-shape = {
            insert = "bar";
            normal = "block";
            select = "underline";
          };
          cursorcolumn = true;
          cursorline = true;
          indent-guides = {
            character = "╎";
            render = true;
            skip-levels = 1;
          };
          line-number = "relative";
          rulers = [ 100 ];
          true-color = true;
          statusline = {
            left = [
              "mode"
              "file-name"
              "position"
              "read-only-indicator"
              "file-modification-indicator"
            ];
            right = [
              "spinner"
              "diagnostics"
              "selections"
              "register"
              "file-encoding"
            ];
            mode = {
              normal = "󰚄";
              insert = "󰏪";
              select = "󰈈";
            };
          };
        };

        keys = import ./keymap.nix;
      };

      # base16_default, but with darker ui elements
      themes.base16_default_modded = import ./theme.nix { inherit (config.definitions) theme; };

      languages = {
        language = [
          {
            name = "any";
            scope = "source.any";
            file-types = [
              "*"
            ];
            indent = {
              unit = "\\t";
              tab-width = 4;
            };
          }
          {
            name = "nix";
            formatter.command = "nixfmt";
          }
        ];
      };
    };

    home.packages =
      with pkgs;
      [
        nil
        bash-language-server
        yaml-language-server
        vscode-langservers-extracted
      ]
      ++ lib.optionals (!cfg.minimal) [
        clang-tools
        mesonlsp
        tinymist
        ruff
        haskell-language-server
      ];
  };
}
