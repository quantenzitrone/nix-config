{
  lib,
  writeShellScript,
  feh,
}:
writeShellScript "fehrandomwallpaper" ''
  #!/usr/bin/env bash
  shopt -s nullglob

  if test "''${1:-""}" != ""; then
  	wallpaperfolder="$1"
  else
  	wallpaperfolder="."
  fi

  # try to go the specified directory
  cd "$wallpaperfolder" || exit 1

  # get list of wallpapers
  files=(*.png *.jpg *.jpeg *.svg *.PNG *.JPG *.JPEG *.SVG)

  # check if the given folder contains any wallpapers
  if test "''${files[1]:-""}" == ""; then
  	echo "$wallpaperfolder is expected to contain images of png jpg jpeg or svg type"
  	exit 1
  fi

  # select a random wallpaper
  index=$((RANDOM % +''${#files[@]}))
  file="''${files[$index]}"

  # apply the wallpaper
  ${lib.getExe feh} --no-fehbg --bg-fill "$file"
''
