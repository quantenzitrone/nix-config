{
  cfg,
  config,
  lib,
  pkgs,
  ...
}:
{
  options = {
    enable = lib.mkEnableOption "zitrones i3 config";
  };

  config = lib.mkIf cfg.enable {
    xsession.windowManager.i3 = {
      enable = true;
      config = import ../sway/config.nix {
        inherit
          cfg
          lib
          pkgs
          ;
        inherit (config) definitions;
        i3Compat = true;
      };
      extraConfig = ''
        exec ${pkgs.writeShellScript "i3-systemd-startup" ''
          ${pkgs.dbus}/bin/dbus-update-activation-environment --systemd \
            DISPLAY I3SOCK XAUTHORITY XDG_CURRENT_DESKTOP XCURSOR_THEME XCURSOR_SIZE

          systemctl --user reset-failed \
            && systemctl --user start i3-session.target \
            && i3-msg -mt subscribe '[]' || true \
            && systemctl --user stop i3-session.target
        ''}

        exec ${pkgs.callPackage ./wallpaper.nix { }} ${pkgs.wallpapers}/purple-collection
      '';
    };

    systemd.user.targets.i3-session = {
      Unit = {
        Description = "i3wm session";
        Documentation = [ "man:systemd.special(7)" ];
        BindsTo = [ "graphical-session.target" ];
        Wants = [
          "graphical-session-pre.target"
          "xdg-desktop-autostart.target"
        ];
        After = [ "graphical-session-pre.target" ];
        Before = [ "xdg-desktop-autostart.target" ];
      };
    };

    home.packages = with pkgs; [
      # cli tools used in this config
      bluetuith
      dunst
      i3lock-fancy-rapid
      libnotify
      redshift
      rofi-wayland
      toggle-touchpad
      # general xorg related tools
      arandr
      feh
      xclip
      xcolor
      xdotool
      xorg.xev
      xorg.xkill
      xorg.xrandr
      xorg.xwininfo
      xpaste
    ];

    services.picom = {
      enable = true;
      startOn = [ "i3-session.target" ];
    };

    zitrone.services = {
      autotiling = {
        enable = true;
        startOn = [ "i3-session.target" ];
      };
      polkit = {
        enable = true;
        startOn = [ "i3-session.target" ];
      };
    };

    # dunst
    systemd.user.services.dunst-i3 =
      let
        dunstrc =
          /*
            lib.generators.toGitINI ../../services/dunst/dunstrc.nix {
              inherit config;
              yoffset = 26;
            }
          */
          "" |> pkgs.writeText "dunstrc";
      in
      {
        Unit = {
          Description = "dunst notification daemon fot i3";
          PartOf = [ "i3-session.target" ];
        };
        Install.WantedBy = [ "i3-session.target" ];
        Service.ExecStart = "${lib.getExe pkgs.dunst} -config ${dunstrc}";
      };

    /*
        # xsettingsd
        qz.userservices.xsettingsd = {
          enable = true;
          startOn = [ "i3.target" ];
        };

        # screen lock
        systemd.user.services.xidlehook-i3 = {
          wantedBy = [ "i3.target" ];
          partOf = [ "i3.target" ];
          script =
            let
              brightnessctl = "${pkgs.brightnessctl}/bin/brightnessctl";
            in
            ''
              ${pkgs.xidlehook}/bin/xidlehook \
                --not-when-fullscreen \
                --timer 240 \
                '${brightnessctl} set $(($(${brightnessctl} get)/4)) >/dev/null' \
                '${brightnessctl} set $(($(${brightnessctl} get)*4)) >/dev/null' \
                --timer 60 \
                '${pkgs.i3lock-fancy-rapid}/bin/i3lock-fancy-rapid 5 pixel' \
                '${brightnessctl} set $(($(${brightnessctl} get)*4)) >/dev/null'
            '';
          serviceConfig = {
            Type = "simple";
            Environment = "DISPLAY=:0";
          };
        };
    */
  };
}
