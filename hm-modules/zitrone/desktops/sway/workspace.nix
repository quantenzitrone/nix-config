{
  lib,
  writeShellScript,
  jq,
  i3Compat ? false,
}:
prev: move:
let
  msgcmd = if i3Compat then "i3-msg" else "swaymsg";

  filter =
    if prev then
      "[(.[] | select(.focused == true).num - 1), 1] | max"
    else
      "[(.[] | select(.focused == true).num + 1), (map(select(.representation != null).num) | max + 1)] | min";

  movecmd =
    if move then
      "${msgcmd} move container to workspace $workspace, workspace $workspace"
    else
      "${msgcmd} workspace $workspace";
in

writeShellScript "workspace" ''
  workspace="$(${msgcmd} -t get_workspaces | ${lib.getExe jq} '${filter}')"

  ${movecmd}
''
