{
  definitions,
  lib,
  pkgs,
  i3Compat,
}:
let
  inherit (definitions.theme) foreground;

  toggle-program =
    name: pkg:
    pkgs.writeShellScript "toggle-${name}" ''
      if pgrep ${name}; then
        pkill ${name}
      else
        ${pkg}/bin/${name}
      fi
    '';
in
pkgs.writers.writeTOML "i3status-rust.toml" {
  theme = {
    overrides = {
      critical_bg = "#00000000";
      good_bg = "#00000000";
      idle_bg = "#00000000";
      info_bg = "#00000000";
      warning_bg = "#00000000";
      separator_bg = "#00000000";
      good_fg = "#${foreground.rgbHex}";
      idle_fg = "#${foreground.rgbHex}";
      info_fg = "#ffff00";
      warning_fg = "#ff8800";
      critical_fg = "#ff0000";
      end_separator = " ";
      separator = "  ";
    };
  };

  icons = {
    icons = "material-nf";
    overrides = {
      bat_charging = [
        "󰢜"
        "󰂆"
        "󰂇"
        "󰂈"
        "󰢝"
        "󰂉"
        "󰢞"
        "󰂊"
        "󰂋"
        "󰂅"
      ];
      memory_mem = [ "" ];
      volume = [ "󰕿" ] ++ (lib.replicate 10 "󰖀") ++ (lib.replicate 10 "󰕾") ++ [ "" ];
      volume_muted = "󰖁";
      coffee_on = "󰅶";
      coffee_off = "󰾪";
    };
  };

  block =
    [
      {
        block = "custom";
        command = "${pkgs.mpc-cli}/bin/mpc current --format '%artist% - %title%'";
        format = " 󰝚 $text.str(max_w:50) ";
        click = [
          {
            button = "left";
            update = true;
          }
          {
            button = "middle";
            cmd = toggle-program "ymuse" pkgs.ymuse;
          }
          {
            button = "right";
            cmd = "${pkgs.mpc-cli}/bin/mpc toggle";
          }
          {
            button = "up";
            cmd = "${pkgs.mpc-cli}/bin/mpc prev";
            sync = true;
            update = true;
          }
          {
            button = "down";
            cmd = "${pkgs.mpc-cli}/bin/mpc next";
            sync = true;
            update = true;
          }
          {
            button = "forward";
            cmd = "${pkgs.mpc-cli}/bin/mpc seek +10";
          }
          {
            button = "back";
            cmd = "${pkgs.mpc-cli}/bin/mpc seek -10";
          }
        ];
      }
      {
        block = "net";
        format = "$icon {$ssid|$device}";
        interval = 10;
        click = [
          {
            button = "left";
            update = true;
          }
          {
            button = "middle";
            cmd = toggle-program "nmtui" "${lib.getExe pkgs.xdg-terminal-exec} ${pkgs.networkmanager}";
          }
        ];
      }
      {
        block = "custom";
        format = " $text.str(max_w:50) ";
        command = "${pkgs.writeShellScript "bluetooth-status" (builtins.readFile ./bluetooth-status.sh)}";
        click = [
          {
            button = "left";
            update = true;
          }
          {
            button = "middle";
            cmd = toggle-program "bluetuith" "${lib.getExe pkgs.xdg-terminal-exec} ${pkgs.bluetuith}";
          }
        ];
      }
      {
        block = "cpu";
        click = [
          {
            button = "left";
            update = true;
          }
          {
            button = "middle";
            cmd = toggle-program "htop" "${lib.getExe pkgs.xdg-terminal-exec} ${pkgs.htop}";
          }
        ];
      }
      {
        block = "nvidia_gpu";
        format = "$icon $utilization";
        click = [
          {
            button = "left";
            update = true;
          }
          {
            button = "middle";
            cmd =
              if i3Compat then toggle-program "arandr" pkgs.arandr else toggle-program "wdisplays" pkgs.wdisplays;
          }
        ];
      }
      {
        block = "memory";
        critical_mem = 80;
        format = "$icon $mem_used_percents";
        warning_mem = 50;
        click = [
          {
            button = "left";
            update = true;
          }
          {
            button = "middle";
            cmd = toggle-program "htop" "${lib.getExe pkgs.xdg-terminal-exec} ${pkgs.htop}";
          }
        ];
      }
      {
        block = "temperature";
        format = "$icon {$max}C";
        good = 50;
        idle = 35;
        info = 65;
        warning = 80;
        click = [
          {
            button = "left";
            update = true;
          }
          {
            button = "middle";
            cmd = toggle-program "htop" "${lib.getExe pkgs.xdg-terminal-exec} ${pkgs.htop}";
          }
        ];
      }
      {
        block = "sound";
        format = "$icon{ $volume|}";
        max_vol = 150;
        click = [
          {
            action = "toggle_mute";
            button = "left";
          }
          {
            button = "middle";
            cmd = toggle-program "pavucontrol" pkgs.pavucontrol;
          }
          {
            button = "down";
            cmd = "${pkgs.graceful}/bin/graceful volume decrease";
            update = true;
          }
          {
            button = "up";
            cmd = "${pkgs.graceful}/bin/graceful volume increase";
            update = true;
          }
        ];
      }
      {
        block = "backlight";
        minimum = 0;
        click = [
          {
            button = "left";
            cmd = "${pkgs.brightnessctl}/bin/brightnessctl set 25%";
            update = true;
          }
          {
            button = "right";
            cmd = "${pkgs.brightnessctl}/bin/brightnessctl set 100%";
            update = true;
          }
          {
            button = "down";
            cmd = "${pkgs.graceful}/bin/graceful brightness decrease";
            update = true;
          }
          {
            button = "up";
            cmd = "${pkgs.graceful}/bin/graceful brightness increase";
            update = true;
          }
        ];
      }
      {
        block = "hueshift";
        click_temp = 3500;
        format = "󰖚 {$temperature}K ";
        hue_shifter = if i3Compat then "redshift" else "wl_gammarelay_rs";
        max_temp = 6500;
        min_temp = 1500;
      }
      {
        block = "battery";
        charging_format = "$icon $percentage";
        critical = 10;
        empty_format = "$icon $percentage";
        full_format = "$icon $percentage";
        good = 30;
        info = 40;
        not_charging_format = "$icon $percentage";
        warning = 20;
        click = [
          {
            button = "left";
            update = true;
          }
        ];
      }
    ]
    ++ lib.optionals (!i3Compat) [
      {
        block = "toggle";
        format = "$icon";
        command_state = pkgs.writeShellScript "swayidle-status" (builtins.readFile ./swayidle-status.sh);
        command_on = "pkill -STOP swayidle";
        command_off = "pkill -CONT swayidle";
        icon_on = "coffee_on";
        icon_off = "coffee_off";
      }
      {
        block = "custom";
        format = "󰋫";
        command = "echo";
        interval = "once";
        click = [
          {
            button = "left";
            cmd = "wpaperctl next";
          }
          {
            button = "right";
            cmd = "wpaperctl previous";
          }
        ];
      }
    ]
    ++ [
      {
        block = "custom";
        command = "date +'%a %c'";
        format = "󰅐 $text.str";
        interval = 1;
      }
    ];
}
