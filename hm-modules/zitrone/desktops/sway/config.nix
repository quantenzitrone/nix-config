{
  cfg,
  definitions,
  lib,
  pkgs,
  i3Compat ? false,
}:
let
  inherit (definitions) theme fonts;
in
lib.optionalAttrs (!i3Compat) {
  input = {
    "type:touchpad" = {
      tap = "enabled";
      natural_scroll = "enabled";
      tap_button_map = "lrm";
      middle_emulation = "enabled";
      drag = "enabled";
    };
    "1:1:AT_Translated_Set_2_keyboard" = {
      xkb_layout = cfg.xkb.layout;
      xkb_variant = cfg.xkb.variant;
      xkb_options = cfg.xkb.options;
    };
  };

  output = {
    eDP-1 = {
      pos = "0 0";
    };
    DP-1 = {
      pos = "0 1080";
    };
  };
}
// {
  colors =
    let
      inactive = lib.mapAttrs (_: x: "#${x.rgbHex}") {
        background = theme.background;
        border = theme.background;
        text = theme.foreground1;
        indicator = theme.background;
        childBorder = theme.background;
      };
    in
    {
      background = "#${theme.background.rgbHex}";
      focused = lib.mapAttrs (_: x: "#${x.rgbHex}") {
        background = theme.background;
        text = theme.foreground;
        border = theme.accent;
        indicator = theme.accent;
        childBorder = theme.accent;
      };
      urgent = lib.mapAttrs (_: x: "#${x.rgbHex}") {
        background = theme.background;
        text = theme.foreground;
        border = theme.termcolors.normal.red;
        indicator = theme.termcolors.normal.red;
        childBorder = theme.termcolors.normal.red;
      };
      focusedInactive = inactive;
      placeholder = inactive;
      unfocused = inactive;
    };

  gaps = {
    outer = 0;
    inner = theme.gaps;
  };

  fonts = {
    names = [ fonts.sansSerif.name ];
    size = 1.0 * fonts.sansSerif.size;
  };

  defaultWorkspace = "workspace number 1";

  window = {
    border = theme.borderWidth;
    titlebar = false;
    hideEdgeBorders = "none";
    commands =
      let
        mkCommand = criteria: command: { inherit criteria command; };
      in
      [
        # force borders
        (mkCommand { all = true; } "border pixel ${toString theme.borderWidth}")
        # inhibit idle when fullscreen
        (mkCommand { all = true; } "inhibit_idle fullscreen")
        # various floaters
        (mkCommand {
          window_role = lib.concatStringsSep "|" [
            "Preferences"
            "bubble"
            "pop-up"
            "task_dialog"
            "dialog"
            "menu"
          ];
        } "floating enable")
        # apps
        (mkCommand (if i3Compat then { class = "flameshot"; } else { app_id = "flameshot"; }) (
          lib.concatStringsSep "," [
            "border pixel 0"
            "floating enable"
            "fullscreen disable"
            "move absolute position 0 0"
          ]
        ))
        (mkCommand (
          if i3Compat then { class = "KeePassXC"; } else { app_id = "org.keepassxc.KeePassXC"; }
        ) "floating enable, sticky enable")
      ];
  };

  floating = {
    border = theme.borderWidth;
    modifier = "Mod4";
    titlebar = false;
  };

  focus = {
    followMouse = true;
    newWindow = "smart";
    mouseWarping = if i3Compat then true else "output";
  };

  bars = lib.singleton (
    {
      position = "bottom";
      fonts = {
        names = [ fonts.sansSerif.name ];
        size = 1.0 * fonts.sansSerif.size;
      };
      statusCommand = "${lib.getExe pkgs.i3status-rust} ${
        import ./i3-status-rust.toml.nix {
          inherit
            definitions
            lib
            pkgs
            i3Compat
            ;
        }
      }";
      colors = rec {
        background = "#00000000";
        inactiveWorkspace = {
          background = background;
          border = background;
          text = "#${theme.foreground.rgbHex}";
        };
        focusedWorkspace = {
          background = "#${theme.accent.rgbHex}";
          border = background;
          text = "#${theme.foreground.rgbHex}";
        };
        activeWorkspace = focusedWorkspace;
        urgentWorkspace = {
          background = "#${theme.termcolors.normal.red.rgbHex}";
          border = background;
          text = "#${theme.foreground.rgbHex}";
        };
      };

      extraConfig = ''
        bindsym button5 exec ${pkgs.callPackage ./workspace.nix { inherit i3Compat; } false false}
        bindsym button4 exec ${pkgs.callPackage ./workspace.nix { inherit i3Compat; } true false}
      '';
    }
    // lib.optionalAttrs i3Compat {
      command = "i3bar -t";
    }
  );

  keybindings =
    let
      moveandswitch =
        x: "move container to workspace number ${toString x}, workspace number ${toString x}";
      sup = "Mod4";
      ly4 = "Mod3";
      ly3 = "Mod5";
    in
    {
      # monitors

      # ┏━━━━━┳━━━┳━━━┳━━━┳━━━┳━━━┓ ┏━━━┳━━━┳━━━┳━━━┳━━━┳━━━━━┓
      # ┃     ┃ q ┃ 󰰙 ┃  ┃ 󰰢 ┃ t ┃ ┃ y ┃ u ┃ i ┃ o ┃ p ┃     ┃
      # ┣━━━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫ ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━━━┫
      # ┃    ┃ a ┃  ┃  ┃  ┃ g ┃ ┃ h ┃ j ┃ k ┃ l ┃ ; ┃    ┃
      # ┣━━━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫ ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━━━┫
      # ┃  󰘶  ┃ z ┃ x ┃ c ┃ v ┃ b ┃ ┃ n ┃ m ┃ , ┃ . ┃   ┃ 󰘶   ┃
      # ┣━━━┳━┻━┳━┻━┳━┻━┳━┻━━━┻━━━┛ ┗━━━┻━━━┻━┳━┻━┳━┻━┳━┻━┳━━━┫
      # ┃   ┃   ┃   ┃  ┃                     ┃  ┃   ┃   ┃   ┃
      # ┗━━━┻━━━┻━━━┻━━━┻━━━━━━━━━━━━━━━━━━━━━┻━━━┻━━━┻━━━┻━━━┛
      # Sup+Layer4        : switch monitors
      # Sup+Layer4+Layer3 : take focused workspace with
      # Sup+Layer4+Shift  : take focused window with

      # focus between monitors
      "${sup}+${ly4}+w" = "output primary";
      "${sup}+${ly4}+e" = "output up";
      "${sup}+${ly4}+r" = "output nonprimary";
      "${sup}+${ly4}+s" = "output left";
      "${sup}+${ly4}+d" = "output down";
      "${sup}+${ly4}+f" = "output right";

      # move workspaces between monitors
      "${sup}+${ly4}+${ly3}+w" = "move workspace to output primary, output primary";
      "${sup}+${ly4}+${ly3}+e" = "move workspace to output up, output up";
      "${sup}+${ly4}+${ly3}+r" = "move workspace to output nonprimary, output nonprimary";
      "${sup}+${ly4}+${ly3}+s" = "move workspace to output left, output left";
      "${sup}+${ly4}+${ly3}+d" = "move workspace to output down, output down";
      "${sup}+${ly4}+${ly3}+f" = "move workspace to output right, output right";

      # move windows between monitors
      "${sup}+${ly4}+Shift+w" = "move container to output primary, output primary";
      "${sup}+${ly4}+Shift+e" = "move container to output up, output up";
      "${sup}+${ly4}+Shift+r" = "move container to output nonprimary, output nonprimary";
      "${sup}+${ly4}+Shift+s" = "move container to output left, output left";
      "${sup}+${ly4}+Shift+d" = "move container to output down, output down";
      "${sup}+${ly4}+Shift+f" = "move container to output right, output right";

      # workspaces
      # ┏━━━━━┳━━━┳━━━┳━━━┳━━━┳━━━┓ ┏━━━┳━━━┳━━━┳━━━┳━━━┳━━━━━┓
      # ┃     ┃ q ┃ w ┃  ┃ r ┃ t ┃ ┃ y ┃ 󰲬 ┃ 󰲮 ┃ 󰲰 ┃ p ┃     ┃
      # ┣━━━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫ ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━━━┫
      # ┃     ┃ a ┃ s ┃ d ┃ f ┃ g ┃ ┃ h ┃ 󰲦 ┃ 󰲨 ┃ 󰲪 ┃ ; ┃     ┃
      # ┣━━━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫ ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━━━┫
      # ┃  󰘶  ┃ z ┃ x ┃  ┃ v ┃ b ┃ ┃ n ┃ 󰲠 ┃ 󰲢 ┃ 󰲤 ┃   ┃ 󰘶   ┃
      # ┣━━━┳━┻━┳━┻━┳━┻━┳━┻━━━┻━━━┛ ┗━━━┻━━━┻━┳━┻━┳━┻━┳━┻━┳━━━┫
      # ┃   ┃   ┃   ┃  ┃                     ┃  ┃   ┃   ┃   ┃
      # ┗━━━┻━━━┻━━━┻━━━┻━━━━━━━━━━━━━━━━━━━━━┻━━━┻━━━┻━━━┻━━━┛
      # Sup+AltGr : workspace by number
      # Sup       : workspace by direction
      # ~+Shift   : move window with focus

      # changing workspace by number
      "${sup}+${ly4}+m" = "workspace number 1";
      "${sup}+${ly4}+comma" = "workspace number 2";
      "${sup}+${ly4}+period" = "workspace number 3";
      "${sup}+${ly4}+j" = "workspace number 4";
      "${sup}+${ly4}+k" = "workspace number 5";
      "${sup}+${ly4}+l" = "workspace number 6";
      "${sup}+${ly4}+u" = "workspace number 7";
      "${sup}+${ly4}+i" = "workspace number 8";
      "${sup}+${ly4}+o" = "workspace number 9";
      "${sup}+${ly4}+Shift+m" = "${moveandswitch 1}";
      "${sup}+${ly4}+Shift+comma" = "${moveandswitch 2}";
      "${sup}+${ly4}+Shift+period" = "${moveandswitch 3}";
      "${sup}+${ly4}+Shift+j" = "${moveandswitch 4}";
      "${sup}+${ly4}+Shift+k" = "${moveandswitch 5}";
      "${sup}+${ly4}+Shift+l" = "${moveandswitch 6}";
      "${sup}+${ly4}+Shift+u" = "${moveandswitch 7}";
      "${sup}+${ly4}+Shift+i" = "${moveandswitch 8}";
      "${sup}+${ly4}+Shift+o" = "${moveandswitch 9}";

      # moving and changing by direction
      "${sup}+e" = "exec --no-startup-id ${
        pkgs.callPackage ./workspace.nix { inherit i3Compat; } true false
      }";
      "${sup}+d" = "exec --no-startup-id ${
        pkgs.callPackage ./workspace.nix { inherit i3Compat; } false false
      }";
      "${sup}+Shift+e" = "exec --no-startup-id ${
        pkgs.callPackage ./workspace.nix { inherit i3Compat; } true true
      }";
      "${sup}+Shift+d" = "exec --no-startup-id ${
        pkgs.callPackage ./workspace.nix { inherit i3Compat; } false true
      }";

      # windows
      # ┏━━━━━┳━━━┳━━━┳━━━┳━━━┳━━━┓ ┏━━━┳━━━┳━━━┳━━━┳━━━┳━━━━━┓
      # ┃  ?  ┃ 󰰜 ┃ w ┃ e ┃ r ┃ t ┃ ┃ y ┃ ? ┃  ┃ ? ┃ p ┃     ┃
      # ┣━━━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫ ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━━━┫
      # ┃     ┃ a ┃ s ┃ d ┃ f ┃ g ┃ ┃ h ┃  ┃  ┃  ┃ ; ┃     ┃
      # ┣━━━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫ ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━━━┫
      # ┃  󰘶  ┃ z ┃ x ┃ c ┃ v ┃ b ┃ ┃ n ┃ m ┃ , ┃ . ┃   ┃ 󰘶   ┃
      # ┣━━━┳━┻━┳━┻━┳━┻━┳━┻━━━┻━━━┛ ┗━━━┻━━━┻━┳━┻━┳━┻━┳━┻━┳━━━┫
      # ┃   ┃   ┃   ┃  ┃                     ┃   ┃   ┃   ┃   ┃
      # ┗━━━┻━━━┻━━━┻━━━┻━━━━━━━━━━━━━━━━━━━━━┻━━━┻━━━┻━━━┻━━━┛
      # Sup : change focus
      "${sup}+i" = "focus up";
      "${sup}+j" = "focus left";
      "${sup}+k" = "focus down";
      "${sup}+l" = "focus right";
      "${sup}+u" = "focus parent";
      "${sup}+o" = "focus child";
      "${sup}+m" = "focus mode_toggle";
      # Sup+Shift : move
      "${sup}+Shift+i" = "move up";
      "${sup}+Shift+j" = "move left";
      "${sup}+Shift+k" = "move down";
      "${sup}+Shift+l" = "move right";
      "${sup}+Shift+u" = "layout tabbed"; # "move into parent"
      "${sup}+Shift+o" = "layout split"; # "move all out of stack"
      "${sup}+Shift+m" = "floating toggle"; # "move into floating"
      # Sup+Ctrl : resize
      "${sup}+Control+i" = "resize shrink height 10 px or 10 ppt";
      "${sup}+Control+j" = "resize shrink width 10 px or 10 ppt";
      "${sup}+Control+k" = "resize grow height 10 px or 10 ppt";
      "${sup}+Control+l" = "resize grow width 10 px or 10 ppt";
      # other
      "${sup}+Tab" = "focus next";
      "${sup}+Shift+Tab" = "focus prev";
      "${sup}+q" = "kill";
      "${sup}+z" = "fullscreen toggle";

      # other
      # ┏━━━━━┳━━━┳━━━┳━━━┳━━━┳━━━┓ ┏━━━┳━━━┳━━━┳━━━┳━━━┳━━━━━┓
      # ┃     ┃ q ┃ w ┃ e ┃ r ┃ t ┃ ┃ y ┃ u ┃ i ┃ o ┃ p ┃     ┃
      # ┣━━━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫ ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━━━┫
      # ┃     ┃ a ┃ s ┃ d ┃ f ┃ g ┃ ┃ h ┃ j ┃ k ┃ l ┃ ; ┃     ┃
      # ┣━━━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫ ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━━━┫
      # ┃  󰘶  ┃ z ┃ x ┃ c ┃ v ┃ b ┃ ┃ n ┃ m ┃ , ┃ . ┃   ┃ 󰘶   ┃
      # ┣━━━┳━┻━┳━┻━┳━┻━┳━┻━━━┻━━━┛ ┗━━━┻━━━┻━┳━┻━┳━┻━┳━┻━┳━━━┫
      # ┃   ┃   ┃   ┃  ┃       space       ┃   ┃   ┃   ┃   ┃
      # ┗━━━┻━━━┻━━━┻━━━┻━━━━━━━━━━━━━━━━━━━━━┻━━━┻━━━┻━━━┻━━━┛

      # notifications
      "${sup}+n" = "exec --no-startup-id ${pkgs.dunst}/bin/dunstctl close";
      "${sup}+Shift+n" = "exec --no-startup-id ${pkgs.dunst}/bin/dunstctl history-pop";

      # lock screen
      "${sup}+Escape" = "exec --no-startup-id loginctl lock-session";

      # app launcher
      "${sup}+space" = "exec \"rofi -modi run,drun -show drun\"";

      # touchpad on/off
      # "${sup}+? input type:touchpad events toggle enabled disabled"

      # keepass
      "${sup}+p" = "exec keepassxc";

      # Screenshot
      "Print" = "exec flameshot gui";

      # apps
      "${sup}+t" = "exec xdg-terminal-exec";
      "${sup}+b" = "exec $BROWSER";
    };
}
