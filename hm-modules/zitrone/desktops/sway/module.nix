{
  cfg,
  config,
  lib,
  osConfig,
  pkgs,
  ...
}:
{
  options = {
    enable = lib.mkEnableOption "zitrones sway config";
    xkb = lib.mkOption {
      type = with lib.types; attrsOf str;
      default = osConfig.services.xserver.xkb;
    };
  };

  config = lib.mkIf cfg.enable {
    wayland.windowManager.sway = {
      enable = true;
      systemd = {
        enable = true;
        xdgAutostart = true;
      };
      config = import ./config.nix {
        inherit
          cfg
          lib
          pkgs
          ;
        inherit (config) definitions;
      };
      extraConfig = ''
        bindgesture swipe:3:up exec --no-startup-id ${pkgs.callPackage ./workspace.nix { } false false}
        bindgesture swipe:3:down exec --no-startup-id ${pkgs.callPackage ./workspace.nix { } true false}
        bindgesture swipe:4:up exec --no-startup-id ${pkgs.callPackage ./workspace.nix { } false true}
        bindgesture swipe:4:down exec --no-startup-id ${pkgs.callPackage ./workspace.nix { } true true}
      ''
      /*
        TODO if swayfx works again
        + ''
          corner_radius ${theme.cornerRadius}
        ''
      */
      ;
    };

    home.packages = with pkgs; [
      # programs from the config
      xdg-terminal-exec
      keepassxc
      dunst
      libnotify

      # from the bar config
      wl-gammarelay-rs
      bluetuith
      networkmanager
      ymuse
      wdisplays
      pavucontrol

      # other general wayland tools
      hyprpicker
      wev
      wl-clipboard
      wlprop
      wlr-randr
      wl-mirror
    ];

    zitrone.services = {
      autotiling = {
        enable = true;
        startOn = [ "sway-session.target" ];
      };
      wpaperd = {
        enable = true;
        startOn = [ "sway-session.target" ];
      };
      swayidle-swaylock = {
        enable = true;
        startOn = [ "sway-session.target" ];
      };
      polkit = {
        enable = true;
        startOn = [ "sway-session.target" ];
      };
      dunst = {
        enable = true;
        startOn = [ "sway-session.target" ];
      };
    };

    zitrone.programs = {
      flameshot.enable = true;
      rofi.enable = true;
      htop.enable = true;
    };
  };
}
