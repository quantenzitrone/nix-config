#!/usr/bin/env bash

# check if bluez is even installed
if ! command -v bluetoothctl >/dev/null; then
	echo '󰂲'
	exit
fi

power=false
if [ "$(bluetoothctl show A0:AF:BD:4A:7A:A3 | grep Powered | awk '{print $2}')" == "yes" ]; then
	power=true
fi

if ! $power; then
	echo '󰂲'
	exit
fi

declare -a devices
mapfile -d '\n' devices < <(bluetoothctl devices Connected | awk '{print $2}' | sed -z 's/\n$//')

# 󰂲  󰂯  󰥰  󰦋  󰏳  󰦢  󰗾
if [ "${#devices[@]}" -eq 0 ]; then
	echo '󰂯' # no device
	exit
else
	case "$(bluetoothctl info "${devices[0]}" | grep Icon | awk '{print $2}' | sed -z 's/\n$//')" in
	audio-headphones | audio-headset) icon='󰥰' ;;
	audio-card) icon='󰦢' ;;
	audio-* | multimedia-*) icon='󰗾' ;;
	input-*) icon='󰦋' ;;
	phone) icon='󰏳' ;;
	*) icon='󰂯' ;;
	esac
fi

echo -n "$icon "

size=${#devices[@]}

for i in $(seq 0 $((size - 2))); do
	bluetoothctl info "${devices[$i]}" | grep Name | awk '{print $2}' | sed -z 's/\n$//'
	echo -n ' & '
done

bluetoothctl info "${devices[size - 1]}" | grep Name | awk '{print $2}' | sed -z 's/\n$//'
