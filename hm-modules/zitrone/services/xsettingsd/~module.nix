{
  config,
  lib,
  pkgs,
  templates,
  ...
}:
let
  cfg = config.qz.userservices.xsettingsd;
in
{
  options.qz.userservices.xsettingsd = {
    enable = lib.mkEnableOption "xsettingsd";
    startOn = lib.mkOption {
      type = with lib.types; listOf str;
      default = [ ];
    };
    variables = {
      inherit (templates.variables)
        font
        theme-cursor
        theme-cursor-package
        theme-gtk
        theme-gtk-package
        theme-icon
        theme-icon-package
        ;
    };
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with cfg.variables; [
      pkgs.xsettingsd
      theme-cursor-package
      theme-gtk-package
      theme-icon-package
    ];

    systemd.user.services.xsettingsd = {
      wantedBy = cfg.startOn;
      partOf = cfg.startOn;
      serviceConfig.ExecStart = "${pkgs.xsettingsd}/bin/xsettingsd -c /etc/xdg/xsettingsd/xsettingsd.conf";
    };

    environment.etc."xdg/xsettingsd/xsettingsd.conf".text = import ./xsettingsd.conf.nix {
      inherit lib;
      inherit (cfg) variables;
    };
  };
}
