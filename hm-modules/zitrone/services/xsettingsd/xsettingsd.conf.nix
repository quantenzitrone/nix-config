{ lib, variables, ... }:
with variables;
let
  mkNameValue =
    name: value:
    "${name} ${
      if lib.isString value then # wrap!
        ''"${value}"''
      else
        toString value
    }";
  toXsettingsd = attrs: lib.mapAttrsToList mkNameValue attrs |> lib.concatStringsSep "\n";
in
toXsettingsd {
  # themes
  "Gtk/CursorThemeName" = "${theme-cursor}";
  "Net/ThemeName" = "${theme-gtk}";
  "Net/IconThemeName" = "${theme-icon}";
  "Net/EnableEventSounds" = 0;
  "Gtk/DecorationLayout" = "icon:";
  # font
  "Gtk/FontName" = with font.sans-serif; "${name}: ${toString size}";
  "Xft/Antialias" = 1;
  "Xft/RGBA" = "rgb";
  "Xft/Hinting" = 1;
  "Xft/HintStyle" = "hintslight";
}
