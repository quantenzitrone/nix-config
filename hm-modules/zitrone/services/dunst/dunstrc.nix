{
  config,
  # due to different behavior on x11 and wayland
  xoffset ? 0,
  yoffset ? 0,
}:
let
  inherit (config.definitions) fonts theme;
in
{
  global = {
    background = "#${theme.background.rgbaHex}";
    corner_radius = theme.cornerRadius;
    enable_recursive_icon_lookup = true;
    follow = "keyboard";
    font = "${fonts.sansSerif.name} ${toString fonts.sansSerif.size}";
    foreground = "#${theme.foreground.rgbHex}";
    format = "<b> %a: %s </b>" + "\n" + "%b";
    frame_color = "#${theme.accent.rgbHex}";
    frame_width = theme.borderWidth;
    height = 200;
    icon_position = "left";
    icon_theme = theme.icons.name;
    max_icon_size = 64;
    min_icon_size = 64;
    mouse_left_click = "do_action";
    mouse_middle_click = "close_all";
    mouse_right_click = "close_current";
    notification_limit = 7;
    offset = "${toString (theme.gaps + xoffset)}x${toString (theme.gaps + yoffset)}";
    origin = "bottom-right";
    separator_color = "auto";
    timeout = 0;
    width = 500;
  };
  urgency_critical = {
    frame_color = "#${theme.termcolors.normal.red.rgbHex}";
  };
  urgency_low = {
    foreground = "#${theme.foreground1.rgbHex}";
  };
}
