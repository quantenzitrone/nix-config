{
  cfg,
  config,
  lib,
  ...
}:
{
  options = {
    enable = lib.mkEnableOption "zitrones dunst config";
    startOn = lib.mkOption {
      type = with lib.types; listOf str;
      default = [ ];
      description = "systemd units to start swayidle on";
    };
  };

  config = lib.mkIf cfg.enable {
    services.dunst = {
      enable = true;
      settings = import ./dunstrc.nix { inherit config; };
    };
    systemd.user.services.dunst = {
      Unit.PartOf = lib.mkOverride 90 cfg.startOn;
      Install.WantedBy = lib.mkOverride 90 cfg.startOn;
    };
  };
}
