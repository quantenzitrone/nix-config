{
  cfg,
  lib,
  pkgs,
  ...
}:
{
  options = {
    enable = lib.mkEnableOption "autotiling service";
    startOn = lib.mkOption {
      type = with lib.types; listOf str;
      default = [ ];
      description = "systemd units to start autotiling on";
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.user.services.autotiling = {
      Unit = {
        Description = "autotiling - daemon to tile sway/i3 automatically";
        PartOf = cfg.startOn;
      };
      Install.WantedBy = cfg.startOn;
      Service.ExecStart = "${lib.getExe pkgs.autotiling} --limit 2";
    };
  };
}
