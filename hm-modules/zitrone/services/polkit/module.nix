{
  cfg,
  lib,
  pkgs,
  ...
}:
{
  options = {
    enable = lib.mkEnableOption "zitrones swayidle & swaylock config";
    startOn = lib.mkOption {
      type = with lib.types; listOf str;
      default = [ ];
      description = "systemd units to start swayidle on";
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.user.services.polkit = {
      Unit = {
        Description = "Polkit Gnome";
        PartOf = cfg.startOn;
      };
      Install.WantedBy = cfg.startOn;
      Service.ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
    };
  };
}
