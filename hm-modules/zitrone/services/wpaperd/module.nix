{
  cfg,
  lib,
  pkgs,
  ...
}:
{
  options = {
    enable = lib.mkEnableOption "wpaperd service";
    wallpaperFolder = lib.mkOption { type = lib.types.path; };
    startOn = lib.mkOption {
      type = with lib.types; listOf str;
      default = [ ];
      description = "systemd units to start flameshot on";
    };
  };

  config = lib.mkIf cfg.enable {
    programs.wpaperd = {
      enable = true;
      settings = {
        default = {
          path = cfg.wallpaperFolder;
          sorting = "random";
        };
      };
    };

    systemd.user.services.wpaperd = {
      Unit = {
        Description = "wpaperd - wallpaper daemon";
        PartOf = cfg.startOn;
      };
      Install.WantedBy = cfg.startOn;
      Service.ExecStart = lib.getExe pkgs.wpaperd;
    };
  };
}
