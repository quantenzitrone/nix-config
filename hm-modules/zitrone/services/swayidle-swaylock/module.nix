{
  cfg,
  lib,
  pkgs,
  ...
}:
let
  lockCmd = "${lib.getExe pkgs.swaylock-effects} -C ${./swaylock-config}";
  mkEvent = event: command: { inherit event command; };
  mkTimeout = timeout: command: { inherit timeout command; };
  mkTimeoutResume = timeout: command: resumeCommand: { inherit timeout command resumeCommand; };
in
{
  options = {
    enable = lib.mkEnableOption "zitrones swayidle & swaylock config";
    startOn = lib.mkOption {
      type = with lib.types; listOf str;
      default = [ ];
      description = "systemd units to start swayidle on";
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages = [ pkgs.swaylock-effects ];

    services.swayidle = {
      enable = true;
      events = [
        (mkEvent "before-sleep" lockCmd)
        (mkEvent "lock" lockCmd)
      ];
      timeouts = [
        (mkTimeout 300 lockCmd)
        (mkTimeoutResume 600 "swaymsg output '*' dpms off" "swaymsg output '*' dpms on")
      ];
    };

    systemd.user.services.swayidle = {
      Unit.PartOf = lib.mkOverride 90 cfg.startOn;
      Install.WantedBy = lib.mkOverride 90 cfg.startOn;
    };
  };
}
