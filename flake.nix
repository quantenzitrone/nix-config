{
  description = "quantenzitrone's system and server configuration";

  # ooooo o   o oooo. o   o ooooo .ooo.
  #   8   88, 8 8   8 8   8   8   8,,,
  #   8   8 "88 8ooo' 8   8   8    """8
  # oo8oo 8   8 8     'ooo'   8   'ooo'
  inputs = {
    # nixpkgs - the base of everything
    ##################################
    # nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";^
    nixpkgs.url = "git+file:././nixpkgs";

    # flake parts
    #######################
    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };

    # lix - alternative nix implementation
    ######################################
    lix = {
      url = "git+https://git.lix.systems/lix-project/lix";
      flake = false;
    };
    lix-module = {
      url = "git+https://git.lix.systems/lix-project/nixos-module";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.lix.follows = "lix";
    };

    # home-manager for config files in $HOME
    ########################################
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # treefmt.nix - formatting the codebase
    #######################################
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Cosmic Desktop until it is stable and in nixpkgs
    ##################################################
    nixos-cosmic = {
      url = "github:lilyinstarlight/nixos-cosmic";
    };

    # Impermanence for a clean root on every reboot
    ###############################################
    impermanence.url = "github:nix-community/impermanence";

    # various flakes providing packages and modules
    ###############################################
    nix-vscode-extensions = {
      url = "github:nix-community/nix-vscode-extensions";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    programsdb = {
      url = "github:wamserma/flake-programs-sqlite";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-alien = {
      url = "github:thiagokokada/nix-alien";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    BadWebsiteBlocklist = {
      url = "github:popcar2/BadWebsiteBlocklist";
      flake = false;
    };
    mimetypes.url = "git+https://codeberg.org/quantenzitrone/useful-mimetypes";
    typhon-ci.url = "github:typhon-ci/typhon";
  };

  # .ooo. o   o ooooo oooo. o   o ooooo .ooo.
  # 8   8 8   8   8   8   8 8   8   8   8,,,
  # 8   8 8   8   8   8ooo' 8   8   8    """8
  # 'ooo' 'ooo'   8   8     'ooo'   8   'ooo'
  outputs =
    {
      self,
      nixpkgs,
      flake-parts,
      treefmt-nix,
      ...
    }@inputs:

    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = [ "x86_64-linux" ];

      imports = [
        treefmt-nix.flakeModule
      ];

      perSystem =
        {
          lib,
          pkgs,
          system,
          ...
        }:
        {
          # Checks (nix flake check)
          ##########################
          # checks."<name>" = derivation;

          # Packages (nix build .#<name>)
          ###############################
          packages = lib.filterAttrs (
            _: pkg:
            lib.isDerivation pkg
            && !(pkg.meta.isBroken or false)
            && !(builtins.elem system (pkg.meta.badPlatforms or [ ]))
            && builtins.elem system (pkg.meta.platforms or lib.platforms.all)
          ) (import ./packages/packages.nix { inherit pkgs; });

          # Treefmt: formatter and checker
          ################################
          treefmt = {
            projectRootFile = "flake.nix";
            programs = {
              nixfmt.enable = true;
              fish_indent.enable = true;
              shfmt = {
                enable = true;
                indent_size = 0;
              };
              shellcheck = {
                enable = true;
              };
              yamlfmt.enable = true;
              toml-sort.enable = true;
              jsonfmt.enable = true;
            };
          };
        };

      flake = {
        # lib - library functions
        #########################
        lib = import ./lib {
          lib = nixpkgs.lib;
          pkgs = nixpkgs.legacyPackages.x86_64-linux;
        };

        # overlays
        ##########
        overlays = import ./packages/overlays.nix { inherit (nixpkgs) lib; };

        # NixOS modules
        ###############
        # nixosModules."<name>" = { config }: { options = {}; config = {}; };
        # Default module (all in one)
        nixosModules = import ./nixos-modules { inherit (nixpkgs) lib; };

        # home-manager modules
        ######################
        # homeManagerModules."<name>" = { config }: { options = {}; config = {}; };
        homeManagerModules = import ./hm-modules { inherit (nixpkgs) lib; };

        # NixOS configurations
        ######################
        # $ nixos-rebuild <switch/boot/test/...> --flake .#<hostname>
        nixosConfigurations = import ./hosts { inherit inputs; };
      };
    };
}
