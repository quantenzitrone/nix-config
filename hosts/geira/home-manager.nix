{ config, inputs, ... }:
let
  hmConfig =
    { inputs, ... }:
    {
      imports = [
        inputs.self.homeManagerModules."definitions"
        inputs.self.homeManagerModules."zitrone.programs.htop"
        inputs.self.homeManagerModules."zitrone.programs.helix"
        ../common/themes/a0f.nix
      ];
      home.stateVersion = "24.05";
      news.display = "silent";
      programs.home-manager.enable = true;

      zitrone = {
        programs.helix = {
          enable = true;
          minimal = true;
        };
        programs.htop.enable = true;
      };
    };
in
{
  imports = [
    inputs.home-manager.nixosModules.default
  ];

  home-manager = {
    useUserPackages = true;
    useGlobalPkgs = true;
    extraSpecialArgs = { inherit inputs; };
    backupFileExtension = "hm-backup";
    users.${config.definitions.user.name} = hmConfig;
    users.root = hmConfig;
  };
}
