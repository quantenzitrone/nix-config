{ ... }:
{
  fileSystems."/nas" = {
    device = "172.16.128.180:/nas/5234";
    fsType = "nfs";
    options = [ "nofail" ];
  };
}
