{
  inputs,
  lib,
  pkgs,
  ...
}:
{
  # import needed modules
  imports =
    [
      # my own modules
      inputs.self.nixosModules.definitions
      inputs.self.nixosModules."zitrone.programs.fish"
      inputs.self.nixosModules."zitrone.programs.shell-base"

      # from other flakes
      inputs.typhon-ci.nixosModules.default

      # hardware config provided by my vps host
      ./vpsadminos.nix

      # nas at my vps provider
      ./nas.nix

      # root & user
      ../common/user.nix

      # theme
      ../common/themes/a0f.nix

      # impermanence
      inputs.impermanence.nixosModules.impermanence

      # nix
      ./nix.nix

      # use some home-manager
      ./home-manager.nix
    ]
    # services
    ++ (builtins.readDir ./services |> lib.mapAttrs (name: _: ./services/${name}) |> lib.attrValues);

  nixpkgs.overlays = [
    inputs.self.overlays.default
  ];

  # enable some basic packages
  zitrone.programs.fish.enable = true;
  environment.systemPackages = with pkgs; [
    curl
    git
    wget
    file
    ripgrep
  ];

  # configure persistence
  environment.persistence."/persist" = {
    directories = [
      # logs
      "/var/log/"
      # systemd state
      "/var/lib/systemd"
      # persistent uids & gids
      "/var/lib/nixos"
      # idk but seems important
      "/var/lib/nfs"
      "/var/lib/dhcpcd"
    ];
    files = [
      "/etc/machine-id"
      "/var/lib/logrotate.status"
    ];
    users.root = {
      home = "/root";
      directories = [
        ".config"
        ".local"
        ".cache"
      ];
    };
    users.zitrone = {
      directories = [
        ".config"
        ".local"
        ".cache"
      ];
    };
  };

  # timezone
  time.timeZone = "Europe/Prague";

  # domain
  networking.domain = "quantenzitrone.eu";

  # faster rebuilding
  documentation = {
    enable = false;
    man.enable = false;
    doc.enable = false;
    info.enable = false;
    nixos.enable = false;
  };

  # system
  systemd.extraConfig = ''
    DefaultTimeoutStartSec=900s
  '';

  # basic information
  system.copySystemConfiguration = false;
  system.stateVersion = "24.05";
}
