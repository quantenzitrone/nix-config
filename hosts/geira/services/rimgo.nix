{ config, ... }:
let
  host = "rimgo.${config.networking.domain}";
in
{
  services.rimgo = {
    enable = true;
    settings = {
      PORT = 3001;
      PRIVACY_COUNTRY = "Czech Republic";
      PRIVACY_PROVIDER = "vpsFree.cz";
      PRIVACY_CLOUDFLARE = "0";
      PRIVACY_NOT_COLLECTED = "true";
      PRIVACY_IP = "false";
      PRIVACY_URL = "false";
      PRIVACY_DEVICE = "false";
      PRIVACY_DIAGNOSTICS = "false";
    };
  };
  services.caddy.virtualHosts.${host} = {
    logFormat = "output discard";
    extraConfig = ''
      reverse_proxy localhost:${toString config.services.rimgo.settings.PORT}
    '';
  };
}
