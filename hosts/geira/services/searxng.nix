{
  config,
  inputs,
  lib,
  pkgs,
  ...
}:
let
  host = "search.${config.networking.domain}";
  cfg = config.services.searx;
in
{
  services.searx = {
    enable = true;
    runInUwsgi = true;
    environmentFile = "/var/lib/searxng/environment.env";
    settings = {
      general = {
        instance_name = "search";
        enable_metrics = false;
      };
      server = {
        base_url = "https://${host}";
        secret_key = "@SEARXNG_SECRET@";
        port = 7291;
        method = "GET";
      };
      ui.theme_args.simple_style = "dark";

      enabled_plugins = [ "Hostnames plugin" ];
      hostnames = {
        replace = {
        };
        remove =
          [
            "minecraft\\.fandom\\.com$"
            "minecraft-archive\\.fandom\\.com$"
          ]
          ++ (
            # get lines
            builtins.readFile "${inputs.BadWebsiteBlocklist}/uBlacklist.txt"
            |> lib.splitString "\n"
            # only lines with uls
            |> lib.filter (lib.hasPrefix "*://")
            # remove http(s) prefix & /* suffix
            |> map (x: lib.removePrefix "*://" x |> lib.removeSuffix "/*")
            # only use results that represent a hostname
            |> lib.filter (x: !(lib.hasInfix "/" x))
            # replace *. and . with equivalent regex
            |> map (lib.replaceStrings [ "." "*." ] [ "\\." "(.*\\.)?" ])
            # append $
            |> map (x: "${x}$")
          );
      };

      engines = lib.mapAttrsToList (name: value: { inherit name; } // value) {
        # general
        brave.disabled = false;
        # images
        "bing images".disabled = true;
        "duckduckgo images".disabled = false;
        # videos
        "bing videos".disabled = true;
        ccc-tv.disabled = false;
        peertube.disabled = false;
        mediathekviewweb.disabled = false;
        sepiasearch.disabled = true;
        # news
        "bing news".disabled = true;
        wikinews.disabled = false;
        "yahoo news".disabled = true;
        # it
        ## packages
        "docker hub".disabled = true;
        hoogle.disabled = true;
        "lib.rs".disabled = false;
        ## repos
        bitbucket.disabled = false;
        codeberg.disabled = false;
        sourcehut.disabled = false;
        ## wikis
        gentoo.disabled = true;
        "free software directory".disabled = false;
        ## other
        "lobste.rs".disabled = false;
        "searchcode code".disabled = false;
        # files (disable)
        btdigg.disabled = true;
        piratebay.disabled = true;
        solidtorrents.disabled = true;
        z-library.disabled = true;
        # social media (disable)
        reddit.disabled = true;
      };
    };
    uwsgiConfig = {
      http = "127.0.0.1:${toString cfg.settings.server.port}";
      cache2 = "name=searxcache,items=2000,blocks=2000,blocksize=65536,bitmap=1";
      buffer-size = 65536;
      env = [ "SEARXNG_SETTINGS_PATH=/run/searx/settings.yml" ];
      disable-logging = true;
    };
  };

  services.caddy.virtualHosts.${host} = {
    extraConfig = ''
      reverse_proxy localhost:${toString cfg.settings.server.port}
      file_server /static/* {
        root ${pkgs.searxng}/share/static
      }
    '';
  };

  environment.persistence."/persist".directories = [
    "/var/lib/searxng"
  ];
}
