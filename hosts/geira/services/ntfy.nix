{ config, lib, ... }:
let
  host = "ntfy.${config.networking.domain}";
  cfg = config.services.ntfy-sh;
in
{
  services.ntfy-sh = {
    enable = true;
    settings = {
      base-url = "https://${host}";
      behind-proxy = true;
      auth-default-access = "deny-all";
      web-root = "disable";
      # use unix socket instead of tcp port (slightly faster)
      listen-http = lib.mkForce "-";
      listen-unix = "/run/ntfy-sh/listen.sock";
      # octal unix listen mode
      listen-unix-mode = (builtins.fromTOML "x = 0o777").x;
    };
  };

  systemd.services.ntfy-sh.serviceConfig = {
    RuntimeDirectory = "ntfy-sh";
  };

  services.caddy.virtualHosts.${host} = {
    extraConfig = ''
      reverse_proxy unix/${cfg.settings.listen-unix}
    '';
  };

  environment.persistence."/persist".directories = [
    "/var/lib/private/ntfy-sh"
  ];
}
