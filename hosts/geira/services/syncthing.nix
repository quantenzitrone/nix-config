{ ... }:
{
  services.syncthing = {
    enable = true;
  };
  # Don't create default ~/Sync folder
  systemd.services.syncthing.environment.STNODEFAULTFOLDER = "true";

  environment.persistence."/persist".directories = [
    "/var/lib/syncthing"
  ];
}
