{ config, ... }:
let
  host = "typhon.${config.networking.domain}";
in
{
  services.typhon = {
    enable = true;
    hashedPassword = "$argon2id$v=19$m=4096,t=3,p=1$bW9CQi90QzQwS1o4YjY0UjBDUk15UT09$z53SkpIhDgZy3fVK8Mo2GvhzaTHilsyBSidcRgYsYHk";
  };
  services.caddy.virtualHosts.${host} = {
    extraConfig = ''
      reverse_proxy localhost:3000 
    '';
  };

  environment.persistence."/persist".directories = [
    "/var/lib/typhon"
  ];
}
