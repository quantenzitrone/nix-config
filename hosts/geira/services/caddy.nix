{ ... }:
{
  services.caddy = {
    enable = true;
    email = "webmaster@quantenzitrone.eu";
  };

  networking.firewall = {
    allowedTCPPorts = [
      80
      443
    ];
  };

  environment.persistence."/persist".directories = [
    {
      directory = "/var/log/caddy";
      user = "caddy";
      group = "caddy";
      mode = "0750";
    }
  ];
}
