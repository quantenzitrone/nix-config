{ inputs, ... }:
{
  services.caddy.virtualHosts."bundesnachrichtendienst.lol" = {
    extraConfig = ''
      ${inputs.self.lib.caddy.httpCat}
      ${inputs.self.lib.caddy.errorGit}

      file_server * {
        root /var/www/homepage
        hide .git
      }
    '';
  };
}
