{ config, inputs, ... }:
{
  services.caddy.virtualHosts."${config.networking.domain}" = {
    extraConfig = ''
      ${inputs.self.lib.caddy.httpCat}
      ${inputs.self.lib.caddy.errorGit}

      file_server * {
        root /var/www/homepage
        hide .git
      }
    '';
  };

  environment.persistence."/persist".directories = [
    "/var/www/homepage"
  ];
}
