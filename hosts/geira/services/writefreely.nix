{ config, ... }:
let
  host = "blog.${config.networking.domain}";
  cfg = config.services.writefreely;
in
{
  services.writefreely = {
    enable = true;
    inherit host;
    settings.server.port = 13519;
    settings.app = {
      site_name = "blog";
      # editor = "bare";
      federation = true;
      single_user = true;
    };
  };

  services.caddy.virtualHosts.${host} = {
    extraConfig = ''
      reverse_proxy localhost:${toString cfg.settings.server.port}
    '';
  };

  environment.persistence."/persist".directories = [
    {
      directory = "/var/lib/writefreely";
      user = "writefreely";
      group = "writefreely";
    }
  ];
}
