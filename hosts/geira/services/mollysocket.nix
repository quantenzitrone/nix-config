{ config, ... }:
let
  host = "mollysocket.${config.networking.domain}";
  cfg = config.services.mollysocket;
in
{
  services.mollysocket = {
    enable = true;
    settings = {
      allowed_endpoints = [ "https://ntfy.quantenzitrone.eu" ];
      allowed_uuids = [ "*" ];
      vapid_key_file = "vapid_key.txt";
    };

  };

  services.caddy.virtualHosts.${host} = {
    extraConfig = ''
      reverse_proxy ${cfg.settings.host}:${toString cfg.settings.port}
    '';
  };

  environment.persistence."/persist".directories = [
    "/var/lib/private/mollysocket"
  ];
}
