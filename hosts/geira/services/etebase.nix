{ config, ... }:
let
  host = "etebase.${config.networking.domain}";
  cfg = config.services.etebase-server;
in
{
  /*
    age.secrets.etebase-server = {
      file = ../secrets/etebase-server.age;
      owner = cfg.user;
      group = cfg.user;
    };
  */

  services.etebase-server = {
    enable = true;
    unixSocket = "${cfg.dataDir}/unixsocket.sock";
    settings = {
      global.secret_file = "${cfg.dataDir}/secret.txt";
      # config.age.secrets.etebase-server.path;
      allowed_hosts.allowed_host1 = host;
    };
  };

  services.caddy.virtualHosts.${host} = {
    extraConfig = ''
      file_server /static/* {
      	root ${cfg.dataDir}
      }
      reverse_proxy unix//${cfg.unixSocket}
    '';
  };

  environment.persistence."/persist".directories = [
    {
      directory = "/var/lib/etebase-server";
      user = "etebase-server";
      group = "etebase-server";
    }
  ];
}
