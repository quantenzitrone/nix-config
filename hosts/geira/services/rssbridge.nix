{ config, ... }:
let
  host = "rssbridge.${config.networking.domain}";
in
{
  services.rss-bridge = {
    enable = true;
    virtualHost = host;
    webserver = "caddy";
    config = {
      system = {
        enabled_bridges = [ "*" ];
        message = "meow :3";
      };
      error.output = "http";
      FileCache.enable_purge = true;
    };
  };

  environment.persistence."/persist".directories = [
    "/var/lib/rss-bridge"
  ];
}
