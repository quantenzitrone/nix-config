{ config, inputs, ... }:
let
  host = "clocks.${config.networking.domain}";
in
{
  services.caddy.virtualHosts.${host} = {
    extraConfig = ''
      ${inputs.self.lib.caddy.httpCat}
      ${inputs.self.lib.caddy.errorGit}

      file_server * {
        root /var/www/clocks
        hide .git
      }
    '';
    serverAliases = [
      "clock.${config.networking.domain}"
    ];
  };

  environment.persistence."/persist".directories = [
    "/var/www/clocks"
  ];
}
