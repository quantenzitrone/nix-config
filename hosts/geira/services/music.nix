{ config, ... }:
let
  host = "music.${config.networking.domain}";
in
{
  services.navidrome = {
    enable = true;
    settings = {
      MusicFolder = "/nas/music";
      PlaylistPath = "/nas/music";
      DataFolder = "/var/lib/navidrome";
      baseUrl = "https://${host}";
      LastFM.Enabled = false;
      ScanSchedule = "@every 10m";
      EnableSharing = true;
      PreferSortTags = true;
    };
  };

  systemd.services.navidrome.after = [ "nas.mount" ];

  services.caddy.virtualHosts.${host} = {
    extraConfig = ''
      reverse_proxy ${config.services.navidrome.settings.Address}:${toString config.services.navidrome.settings.Port}
    '';
  };

  environment.persistence."/persist".directories = [
    {
      directory = "/var/lib/navidrome";
      user = "navidrome";
      group = "navidrome";
    }
  ];
}
