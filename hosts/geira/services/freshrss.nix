{ config, ... }:
let
  cfg = config.services.freshrss;
  host = "rss.${config.networking.domain}";
in
{
  services.freshrss = {
    enable = true;
    baseUrl = "https://${host}";
    virtualHost = host;
    webserver = "caddy";
    defaultUser = "admin";
    passwordFile = "${cfg.dataDir}/password.txt";
    # TODO use age encrypted secret
  };

  environment.persistence."/persist".directories = [
    {
      directory = "/var/lib/freshrss";
      user = "freshrss";
      group = "freshrss";
    }
  ];
}
