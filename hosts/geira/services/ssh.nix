{ config, ... }:
{
  services.openssh = {
    enable = true;
    settings = {
      PermitRootLogin = "yes";
      PasswordAuthentication = false;
      X11Forwarding = false;
    };
  };
  users.users.${config.definitions.user.name}.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGJ0j5BztROUdZYHf8cpJsJr9jd8gCRUfm6oe9k3Bhh0 general@dev.quantenzitrone.eu"
    "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIEHxSon5NqDQ35sRfV8HwPqyhtoJ+jX705jhwSet8qgsAAAADHNzaDpuaXRyb2tleQ== general@dev.quantenzitrone.eu"
  ];
  users.users.root.openssh.authorizedKeys.keys = [
    "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIEHxSon5NqDQ35sRfV8HwPqyhtoJ+jX705jhwSet8qgsAAAADHNzaDpuaXRyb2tleQ== general@dev.quantenzitrone.eu"
  ];

  # persist host keys
  environment.persistence."/persist".files = [
    "/etc/ssh/ssh_host_ed25519_key"
    "/etc/ssh/ssh_host_ed25519_key.pub"
    "/etc/ssh/ssh_host_rsa_key"
    "/etc/ssh/ssh_host_rsa_key.pub"
  ];
}
