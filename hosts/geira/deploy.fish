#!/usr/bin/env fish

if test (count $argv) -eq 0
    set command switch
else
    set command $argv
end

set flakepath (realpath (dirname (status filename))/../..)
set hostname_ (basename (dirname (status filename)))

nix build $flakepath#nixosConfigurations.$hostname_.config.system.build.toplevel \
    --no-link \
    --show-trace \
    --log-format internal-json --verbose &|
    nom --json

and nixos-rebuild $command \
    --flake $flakepath#$hostname_ \
    --target-host root@quantenzitrone.eu
