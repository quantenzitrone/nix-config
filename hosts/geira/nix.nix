{
  inputs,
  lib,
  pkgs,
  ...
}:
{
  imports = [
    inputs.programsdb.nixosModules.programs-sqlite
  ];

  nix = {
    package = pkgs.lix;

    # enable flakes and related
    settings.extra-experimental-features = [
      "flakes"
      "nix-command"
      "repl-flake"
      "pipe-operator"
      "no-url-literals"
    ];

    # disable global registry, bc it is imparative and uncontrollably updated
    settings.flake-registry = "";

    extraOptions = ''
      repl-overlays = ${../common/nix/repl-overlays.nix}
    '';

    # map the flake inputs to the system flake registry
    registry = lib.attrsets.mapAttrs' (name: flake: {
      inherit name;
      value = {
        inherit flake;
      };
    }) inputs;

    # replace channel with NIX_PATH
    channel.enable = true;
    nixPath = lib.mapAttrsToList (name: value: "${name}=${value}") inputs;
  };

  # configure command-not-found to work with flakes
  programs-sqlite.enable = true;
}
