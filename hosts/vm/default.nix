{ inputs }:
let
  inherit (inputs) self nixpkgs programsdb;
in
nixpkgs.lib.nixosSystem rec {
  system = "x86_64-linux";
  modules = [
    # my own modules
    self.nixosModules.default
    # from other flakes
    programsdb.nixosModules.programs-sqlite

    # nix config
    ../common/nix
    # basic system
    ./basic-system.nix
    # modules
    ./modules.nix

    # basic information
    {
      networking.hostName = "vm";
      system.copySystemConfiguration = false;
      system.stateVersion = "24.05";
    }
  ];
  specialArgs = {
    pkgs = import nixpkgs {
      inherit system;
      overlays = [ ];
    };
    inherit inputs;
  };
}
