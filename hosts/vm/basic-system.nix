{ config, pkgs, ... }:
{
  users.mutableUsers = false;

  # user
  users.users.user = {
    isNormalUser = true;
    shell = pkgs.fish;
    # easy password
    password = "";
    extraGroups = [
      "wheel"
      "video"
      "audio"
      "networkmanager"
    ];
  };

  # locale & keymap
  time.timeZone = "Etc/UTC";
  i18n.defaultLocale = "en_DK.UTF-8";
  console.keyMap = "neoqwertz";

  # keymap in X11
  services.xserver.xkb = {
    layout = "de";
    variant = "neo_qwerty";
  };
  environment.variables = with config.services.xserver.xkb; {
    XKB_DEFAULT_LAYOUT = layout;
    XKB_DEFAULT_VARIANT = variant;
  };

  # faster rebuilding
  documentation = {
    enable = false;
    man.enable = false;
    doc.enable = false;
    info.enable = false;
    nixos.enable = false;
  };

  # shell & editor
  zitrone.programs.fish.enable = true;
  environment.systemPackages = with pkgs; [ helix ];

  # filesystems
  fileSystems."/" = {
    device = "/dev/disk/by-label/nixos";
    fsType = "ext4";
  };
  boot.loader.grub.device = "/dev/disk/by-label/nixos";
}
