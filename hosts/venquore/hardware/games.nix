{ ... }:
{
  fileSystems."/games" = {
    device = "/dev/disk/by-label/games";
    fsType = "ext4";
  };
  # symlinking the config files to the users .config and .local/share
  zitrone.userservices.symlinks = {
    ".config/lutris".target = "/games/ConfigHome/config/lutris";
    ".config/heroic".target = "/games/ConfigHome/config/heroic";
    ".config/ppsspp".target = "/games/ConfigHome/config/ppsspp";
    ".config/rpcs3".target = "/games/ConfigHome/config/rpcs3";
    ".local/share/lutris".target = "/games/ConfigHome/localshare/lutris";
    ".local/share/PolyMC".target = "/games/ConfigHome/localshare/PolyMC";
  };
}
