{ lib, ... }:
{
  # mounting the data partition
  boot.initrd.luks.devices.data = {
    device = "/dev/disk/by-uuid/c4e91eaa-0ad0-4e84-8f12-0c7e49b5067b";
    # firstly try unlocking with fido2
    crypttabExtraOpts = [
      "fido2-device=auto"
      "token-timeout=10"
    ];
    # then try use the fist flash drive (sda is internal)
    keyFile = "/dev/sdb";
    keyFileSize = 4096;
    keyFileOffset = 512; # size of MBR, so the flash drive is usable for other things
    keyFileTimeout = 5;
    # else password is used
  };

  fileSystems."/data" = {
    device = "/dev/disk/by-label/data";
    fsType = "ext4";
  };

  # link folders into the home directories
  zitrone.userservices.symlinks = {
    "documents".target = "/data/documents";
    "internet".target = "/data/internet";
    "music".target = "/data/music";
    "pictures".target = "/data/pictures";
    "videos".target = "/data/videos";
    ".secret".target = "/data/.secret";
    ".ssh".target = "/data/.ssh";
    ".uselessfolder".target = "/data/.uselessfolder";
  };

  # set xdg user dirs
  environment.etc."xdg/user-dirs.defaults".text =
    lib.generators.toKeyValue { mkKeyValue = (n: v: "${lib.toUpper n}=${v}"); }
      {
        documents = "documents";
        download = "internet";
        pictures = "pictures";
        music = "music";
        videos = "videos";
        # bruv who needs these?
        desktop = ".uselessfolder";
        templates = ".uselessfolder";
        publicshare = ".uselessfolder";
      };
  # shortcut variables for some folders
  environment.variables = {
    uni = "/data/documents/education/University";
    prog = "/data/documents/programming";
  };
}
