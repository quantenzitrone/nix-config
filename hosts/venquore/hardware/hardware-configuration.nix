{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}:
{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  ########################################
  # kernel modules
  boot.initrd.availableKernelModules = [
    # for LUKS
    "aesni_intel"
    "cryptd"
    # was here by default
    "xhci_pci"
    "ahci"
    "nvme"
    "usb_storage"
    "sd_mod"
    "rtsx_pci_sdmmc"
  ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  ########################################
  # file systems
  boot.initrd.systemd.enable = true;

  boot.initrd.luks.devices.cryptroot = {
    device = "/dev/disk/by-uuid/2fa7d802-8f9f-4efa-9dcd-99e8be509d30";
    allowDiscards = true;
    bypassWorkqueues = true;
    # firstly try unlocking with fido2
    crypttabExtraOpts = [
      "fido2-device=auto"
      "token-timeout=10"
    ];
    # then try use the fist flash drive (sda is internal)
    keyFile = "/dev/sdb";
    keyFileSize = 4096;
    keyFileOffset = 512; # size of MBR, so the flash drive is usable for other things
    keyFileTimeout = 5;
    # else password is used
  };
  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-label/BOOT";
      fsType = "vfat";
    };
    "/" = {
      device = "/dev/disk/by-label/nixos";
      fsType = "ext4";
    };
  };
  swapDevices = lib.singleton {
    device = "/swapfile";
    size = 15891;
  };

  # use the latest linux kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;

  ########################################
  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.eno1.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlo1.useDHCP = lib.mkDefault true;

  ########################################
  # other hardware

  hardware.enableRedistributableFirmware = true;

  # cpu
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  # ratbagd for gaming mouse
  services.ratbagd.enable = true;

  # nvidia proprietary drivers
  services.xserver.videoDrivers = [
    "nvidia"
    "modesetting"
  ];
  hardware.graphics.enable32Bit = true;
  hardware.nvidia = {
    package = config.boot.kernelPackages.nvidiaPackages.production;
    # modesetting breaks xorg on my gpu
    # see https://github.com/NixOS/nixpkgs/pull/326369#issuecomment-2437964243
    modesetting.enable = lib.mkForce false;
  };

  # nitrokey
  hardware.nitrokey.enable = true;

  # opentabletdriver for xp-pen tablet
  hardware.opentabletdriver.enable = true;
  services.udev.extraRules = ''
    SUBSYSTEM=="hidraw", ATTRS{idVendor}=="28bd", ATTRS{idProduct}=="094a", MODE="0666"
    SUBSYSTEM=="usb", ATTRS{idVendor}=="28bd", ATTRS{idProduct}=="094a", MODE="0666"
    SUBSYSTEM=="input", ATTRS{idVendor}=="28bd", ATTRS{idProduct}=="094a", ENV{LIBINPUT_IGNORE_DEVICE}="1"
  '';

  # touchpad
  services.libinput = {
    enable = true;
    touchpad = {
      tapping = true;
      naturalScrolling = true;
      horizontalScrolling = true;
      disableWhileTyping = true;
    };
    mouse.middleEmulation = false;
  };

  # qmk keyboard
  hardware.keyboard.qmk.enable = true;

  # bluetooth
  hardware.bluetooth.enable = true;
  hardware.bluetooth.settings.General.Enable = "Source,Sink,Media,Socket";
}
