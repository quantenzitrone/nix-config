{
  inputs,
  lib,
  pkgs,
  ...
}:
{
  imports = [
    # import my own modules
    inputs.self.nixosModules.default

    # import other modules
    inputs.nixos-cosmic.nixosModules.default

    # set up lix
    inputs.lix-module.nixosModules.default
    ../common/nix

    # result of the hardware scan + tweaks & file systems
    ./hardware/hardware-configuration.nix
    ./hardware/data.nix
    ./hardware/games.nix

    # set up users
    ../common/user.nix

    # git with name and email
    ../common/git.nix
    { programs.git.config.commit.gpgSign = true; }

    # allow some unfree packages
    ./unfree.nix

    # stuff
    ./modules
    ./stuff/uni-vpn.nix
    ./stuff/uni-mail.nix

    # import overlays
    {
      nixpkgs.overlays = [
        # my public overlays
        inputs.self.overlays.default

        # imported overlays
        (final: prev: {
          vscode-extensions =
            prev.vscode-extensions // inputs.nix-vscode-extensions.extensions.${prev.system};
        })
        inputs.nix-alien.overlays.default
      ];
    }

  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };

  # locale & keymap
  time.timeZone = "Europe/Berlin";
  i18n = {
    defaultLocale = "en_DK.UTF-8";
    extraLocaleSettings = {
      LC_MONETARY = "de_DE.UTF-8";
    };
    supportedLocales = [ "all" ];
  };

  console.keyMap = "neoqwertz";
  # keymap in X11
  services.xserver.xkb = {
    layout = "de,de,de,us";
    variant = "neo_qwerty,bone,,";
    options = lib.concatStringsSep "," [
      "compose:102"
      "compose:rwin"
      "altwin:swap_lalt_lwin"
    ];
  };

  # some basic backages
  environment.systemPackages = with pkgs; [
    git
    wget
    bat
  ];

  # networking with adblock hosts
  networking = {
    networkmanager.enable = true;
    firewall.enable = true;
    stevenblack = {
      enable = true;
      block = [
        "fakenews"
        "gambling"
      ];
    };
  };

  # stuff
  system.copySystemConfiguration = false;
  system.stateVersion = "25.05";
}
