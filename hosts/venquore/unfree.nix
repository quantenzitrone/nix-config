{ lib, ... }:
{
  # only allow some unfree packages
  nixpkgs.config.allowUnfreePredicate =
    pkg:
    lib.elem (lib.getName pkg) [
      # gaming
      "steam"
      "steam-unwrapped"
      "osu-lazer" # only libbass is proprietary
      # fuck nvidia:
      "nvidia-x11"
      "nvidia-settings"
      "cudaPackages.cudatoolkit"
    ];
}
