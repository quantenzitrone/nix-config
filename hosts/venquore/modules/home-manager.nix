{ config, inputs, ... }:
let
  mkHmConfig =
    configuration:
    { config, osConfig, ... }:
    {
      imports = [
        inputs.self.homeManagerModules.default
        ../../common/themes/da-one.nix
        configuration
      ];
      definitions = {
        inherit (osConfig.definitions) fonts user;
        theme = {
          inherit (osConfig.definitions.theme)
            borderWidth
            gaps
            icons
            cornerRadius
            ;
        };
      };
      home.stateVersion = "24.05";
      news.display = "silent";
      programs.home-manager.enable = true;
    };
in
{
  imports = [
    inputs.home-manager.nixosModules.default
  ];

  home-manager = {
    useUserPackages = true;
    useGlobalPkgs = true;
    backupFileExtension = "hm-backup";
    extraSpecialArgs = { inherit inputs; };
    users.${config.definitions.user.name} = mkHmConfig (
      { pkgs, ... }:
      {
        zitrone = {
          programs.librewolf.enable = true;
          programs.helix.enable = true;
          programs.htop.enable = true;
          programs.cli-visualizer.enable = true;
          programs.xournalpp.enable = true;
          programs.flameshot.savePath = "/data/pictures/screenshots";
          services.wpaperd.wallpaperFolder = "${pkgs.wallpapers}/purple-collection";
        };
      }
    );

    users.root = mkHmConfig (
      { pkgs, ... }:
      {
        zitrone = {
          programs.helix.enable = true;
          programs.htop.enable = true;
          programs.cli-visualizer.enable = true;
          services.wpaperd.wallpaperFolder = "${pkgs.wallpapers}/linux-chan-collection";
        };
      }
    );
  };
}
