{
  inputs,
  lib,
  pkgs,
  ...
}:
let
  inherit (inputs.mimetypes.lib) mimetypes;
in
{
  environment.systemPackages = with pkgs; [
    # for gtk-launch
    gtk3
  ];

  # enable mime app configuration
  xdg.mime.enable = true;

  # enable portals for integration
  xdg.portal = {
    enable = true;
    wlr.enable = true;
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };

  imports = [
    # TERMINAL
    ##############################
    {

      zitrone.programs.alacritty.enable = true;
      environment.systemPackages = [
        (pkgs.writeTextFile {
          name = "alacriity-directory-desktop";
          destination = "/share/applications/alacritty-directory.desktop";
          text = ''
            [Desktop Entry]
            Name=Alacritty
            GenericName=Terminal
            Icon=Alacritty
            StartupWMClass=Alacritty
            Type=Application
            Categories=System;TerminalEmulator;
            TryExec=alacritty
            Exec=alacritty --working-directory %U
            StartupNotify=false
            MimeType=inode/directory
          '';
        })
      ];
      # enable the proposed xdg-terminal-exec spec
      xdg.terminal-exec = {
        enable = true;
        settings = {
          default = [ "Alacritty.desktop" ];
        };
        package = pkgs.xdg-terminal-exec-mkhl;
      };
      environment.variables.TERMINAL = "alacritty";

      xdg.mime.addedAssociations."inode/directory" = lib.mkForce [ "alacritty-directory.desktop" ];
    }
    # EDITOR
    ##############################
    {
      # helix
      # config see home-manager
      # fallback
      environment.systemPackages = with pkgs; [ micro ];
      environment.variables = {
        EDITOR = "hx";
        VISUAL = "hx";
      };
      xdg.mime =
        let
          associations = (
            lib.genAttrs mimetypes.text (_: [
              "Helix.desktop"
              "micro.desktop"
            ])
          );
        in
        {
          defaultApplications = associations;
          addedAssociations = associations;
        };
    }
    # FILE MANAGER
    ##############################
    {
      environment.systemPackages = [ pkgs.nautilus ];
      environment.variables.FILEMANAGER = "nautilus";
      xdg.mime =
        let
          associations = lib.genAttrs [
            "inode/directory"
            "inode/mount-point"
            "x-scheme-handler/ssh"
            "x-scheme-handler/smb"
            "x-scheme-handler/nfs"
            "x-scheme-handler/ftp"
            "x-scheme-handler/ptp"
            "x-scheme-handler/mtp"
          ] (_: [ "org.gnome.Nautilus.desktop" ]);
        in
        {
          addedAssociations = associations;
          defaultApplications = associations;
        };
    }
    # AUDIO & VIDEO
    ##############################
    {
      zitrone.programs.mpv = {
        enable = true;
        screenshotPath = "/data/pictures/screenshots";
      };
      environment.variables = {
        AUDIOPLAYER = "mpv";
        VIDEOPLAYER = "mpv";
      };
      xdg.mime =
        let
          associations =
            (lib.genAttrs inputs.self.lib.xdg."audio.nix" (_: [ "mpv.desktop" ]))
            // (lib.genAttrs inputs.self.lib.xdg."video.nix" (_: [ "mpv.desktop" ]));
        in
        {
          addedAssociations = associations;
          defaultApplications = associations;
        };
    }
    # IMAGE VIEWER
    ##############################
    {
      zitrone.programs.imv.enable = true;
      environment.variables.IMAGEVIEWER = "imv-dir";
      xdg.mime =
        let
          associations = lib.genAttrs mimetypes.image (_: [ "imv-dir.desktop" ]);
        in
        {
          addedAssociations = associations;
          defaultApplications = associations;
        };
    }
    # DOCUMENT VIEWER
    ##############################
    {
      environment.systemPackages = with pkgs; [ evince ];
      environment.variables.PDFVIEWER = "evince";
      xdg.mime.defaultApplications =

        lib.genAttrs [
          "application/illustrator"
          "application/oxps"
          "application/pdf"
          "application/postscript"
          "application/vnd.comicbook+zip"
          "application/vnd.comicbook-rar"
          "application/vnd.ms-xpsdocument"
          "application/x-bzdvi"
          "application/x-bzpdf"
          "application/x-bzpostscript"
          "application/x-cb7"
          "application/x-cbr"
          "application/x-cbt"
          "application/x-cbz"
          "application/x-dvi"
          "application/x-ext-cb7"
          "application/x-ext-cbr"
          "application/x-ext-cbt"
          "application/x-ext-cbz"
          "application/x-ext-djv"
          "application/x-ext-djvu"
          "application/x-ext-dvi"
          "application/x-ext-eps"
          "application/x-ext-pdf"
          "application/x-ext-ps"
          "application/x-gzdvi"
          "application/x-gzpdf"
          "application/x-gzpostscript"
          "application/x-xzpdf"
        ] (_: [ "org.gnome.Evince.desktop" ]);
    }
    # BROWSER
    ##############################
    {
      # see ../home-manager/librewolf/ for program config
      environment.variables.BROWSER = "librewolf";
      xdg.mime.defaultApplications = lib.genAttrs [
        "application/xhtml+xml"
        "application/vnd.mozilla.xul+xml"
        "x-scheme-handler/http"
        "x-scheme-handler/https"
      ] (_: [ "librewolf.desktop" ]);
    }
    # OTHER
    ##############################
    {
      environment.systemPackages = with pkgs; [
        gnome-font-viewer
        fragments
      ];

      # set default mime applications
      xdg.mime.defaultApplications =
        (lib.genAttrs inputs.self.lib.xdg."font.nix" (_: [ "org.gnome.FontViewer.desktop" ]))
        // {
          # torrent
          "application/x-bittorrent" = [ "de.haeckerfelix.Fragments.desktop" ];
          "x-scheme-handler/magnet" = [ "de.haeckerfelix.Fragments.desktop" ];
        };
    }
  ];
}
