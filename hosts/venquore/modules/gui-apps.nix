{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    # browsers
    tangram
    epiphany
    tor-browser-bundle-bin
    # file managing stuff
    xdragon
    # media
    tenacity
    gimp
    inkscape
    peek
    krita
    obs-studio
    # settings stuff
    dconf-editor
    piper
    gparted
    gnome-disk-utility
    pavucontrol
    # office
    libreoffice
    # drawio
    # messaging
    thunderbird
    signal-desktop
    telegram-desktop
    element-desktop
    fractal
    # password manager
    keepassxc
  ];
}
