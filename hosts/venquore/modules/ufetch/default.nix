{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [ ufetch-fish ];
  environment.etc."xdg/ufetch/nixos.fish".text = builtins.readFile ./nixos.fish;
}
