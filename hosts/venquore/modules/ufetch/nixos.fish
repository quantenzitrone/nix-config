set color1 $red
set color2 $yellow
set color3 $green
set color4 $cyan
set color5 $blue
set color6 $magenta
function parse_uptime
    echo (math $argv[1] / 1000)ks
end
