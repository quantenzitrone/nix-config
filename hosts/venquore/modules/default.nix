{
  lib,
  pkgs,
  config,
  ...
}:
{
  imports = [

    # applications
    ./defaultapps.nix
    ./programming.nix
    ./games.nix
    ./music.nix
    ./cli-utils.nix
    ./gui-apps.nix
    ./ufetch
    ./home-manager.nix

    ## theme
    ########
    {
      imports = [
        ../../common/themes/da-one.nix
      ];
      definitions.theme = {
        borderWidth = 2;
        cornerRadius = 10;
        gaps = 5;
        icons = {
          name = "Papirus-Dark";
          package = pkgs.papirus-icon-theme.override { color = "orange"; };
        };
      };
      environment.systemPackages = [ config.definitions.theme.icons.package ];
    }
    {
      # cursor
      environment.etc."xdg/cursor-theme/index.theme".text = ''
        [Icon Theme]
        Name=Default
        Inherits=Bibata-Modern-Ice
        ThemeInherits=Bibata-Modern-Ice
      '';
      zitrone.userservices.symlinks.".icons/default".target = "/etc/xdg/cursor-theme";
      environment.systemPackages = [ pkgs.bibata-cursors ];
      programs.dconf.profiles.user.databases = lib.singleton {
        lockAll = true;
        settings."org.gnome.desktop.interface" = {
          cursor-size = lib.gvariant.mkInt32 24;
          cursor-theme = "Bibata-Modern-Ice";
        };
      };
      environment.variables.XCURSOR_THEME = "Bibata-Modern-Ice";
    }
    {
      # qt theming
      qt = {
        enable = true;
        platformTheme = "gtk2";
        style = "gtk2";
      };
      # gtk theming
      environment.systemPackages = [ (pkgs.orchis-theme.override { tweaks = [ "black" ]; }) ];
      programs.dconf.profiles.user.databases = lib.singleton {
        lockAll = true;
        settings = {
          "org.gnome.desktop.interface" = {
            color-scheme = "prefer-${config.definitions.theme.themeType}";
            gtk-theme = "Orchis-Purple-Dark";
            icon-theme = config.definitions.theme.icons.name;
          };
          "org.gnome.desktop.wm.preferences".button-layout = "icon:";
        };
      };
    }

    ## font configuration
    #####################
    {
      definitions.fonts = {
        sansSerif.size = 12;
        monospace.size = 13;
        serif.size = 12;
      };

      fonts = {
        packages = with pkgs; [
          # nice document font
          liberation_ttf
          # main fonts
          nerd-fonts.ubuntu-sans
          unifont
          # emoji
          twitter-color-emoji
          noto-fonts-emoji
        ];
        fontconfig = {
          enable = true;
          defaultFonts = rec {
            sansSerif = [
              "UbuntuSans Nerd Font"
              "Unifont"
            ];
            monospace = [
              "UbuntuSansMono Nerd Font"
              "Unifont"
            ];
            emoji = [
              "Twitter Color Emoji"
              "Noto Color Emoji"
            ];
            serif = sansSerif;
          };
        };
      };
      programs.dconf.profiles.user.databases = lib.singleton {
        lockAll = true;
        settings."org.gnome.desktop.interface" = {
          font-name = "${config.definitions.fonts.sansSerif.name} ${toString config.definitions.fonts.sansSerif.size}";
          monospace-font-name = "${config.definitions.fonts.monospace.name} ${toString config.definitions.fonts.monospace.size}";
        };
      };
    }

    ##############################
    ##### System Services
    ##############################
    {
      services.actkbd.brightness = {
        enable = true;
        upCommand = "${pkgs.graceful}/bin/graceful brightness up";
        downCommand = "${pkgs.graceful}/bin/graceful brightness down";
      };

      zitrone.services.pipewire = {
        enable = true;
        user = config.definitions.user.name;
      };

      services.displayManager.cosmic-greeter.enable = true;
      services.greetd.settings.default_session.command = lib.mkForce (
        pkgs.writeShellScript "cosmic-greeter" ''
          export XKB_DEFAULT_LAYOUT=${lib.escapeShellArg config.services.xserver.xkb.layout}
          export XKB_DEFAULT_VARIANT=${lib.escapeShellArg config.services.xserver.xkb.variant}
          export XKB_DEFAULT_OPTIONS=${lib.escapeShellArg config.services.xserver.xkb.options}
          export XCURSOR_THEME="''${XCURSOR_THEME:-Pop}"

          systemd-cat -t cosmic-greeter ${lib.getExe pkgs.cosmic-comp} ${lib.getExe pkgs.cosmic-greeter}
        ''
      );

      zitrone.services.mpd = {
        enable = true;
        user = config.definitions.user.name;
        musicDirectory = "/data/music/hqmusic";
        playlistDirectory = "/data/music/hqmusic";
      };

      # hidden wifi hotspot (enable when needed)
      services.create_ap = {
        enable = false;
        settings = {
          WIFI_IFACE = "wlo1";
          INTERNET_IFACE = "eno1";
          SSID = "mRNA-Impfchip-WHO2309872c";
          HIDDEN = "1";
        };
      };

      # vpn
      services.mullvad-vpn = {
        enable = true;
        package = pkgs.mullvad-vpn;
      };

      # flatpak
      services.flatpak.enable = true;

      # mount MTP devices automatically
      services.gvfs.enable = true;
    }

    ##############################
    ##### Programs
    ##############################
    {
      zitrone.programs.fish.enable = true;
    }

    ##############################
    ##### Desktops
    ##############################
    {
      zitrone.desktops.cosmic.enable = true;
      zitrone.desktops.sway = {
        enable = true;
        users = [ config.definitions.user.name ];
      };
      zitrone.desktops.i3 = {
        enable = true;
        users = [ config.definitions.user.name ];
      };
      zitrone.desktops.gtty = {
        enable = true;
        users = [
          config.definitions.user.name
          "root"
        ];
      };
    }
    {
      # nitrokey
      environment.systemPackages = with pkgs; [
        pynitrokey
        nitrokey-app2
      ];
      programs.gnupg.agent.enable = true;

      # polkit
      security.polkit.enable = true;
    }
    {
      # syncthing
      services.syncthing = {
        enable = true;
        systemService = false;
      };
      environment.systemPackages = [ pkgs.syncthing ];

      # Don't create default ~/Sync folder
      systemd.services.syncthing.environment.STNODEFAULTFOLDER = "true";
    }
    {
      # dconf
      programs.dconf.enable = true;

      # ssh
      programs.ssh.startAgent = true;

      # gnome-keyring
      services.gnome.gnome-keyring.enable = true;
      programs.seahorse.enable = true;

      # virt-manager and qemu
      virtualisation.libvirtd.enable = true;
      programs.virt-manager.enable = true;
    }
  ];
}
