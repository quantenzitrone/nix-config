{ pkgs, ... }:
{
  imports = [ ../../common/git.nix ];
  programs.git.config.commit.gpgSign = true;
  environment.systemPackages = with pkgs; [
    # oooo. oooo. ,ooo, ,ooo, oooo.  ,8,  8, ,8 8, ,8 ooooo 8,  8 ,ooo,
    # 8   8 8   8 8   8 8     8   8 8   8 8 8 8 8 8 8   8   8o, 8 8
    # 8ooo' 8ooo' 8   8 8  "8 8ooo' 8ooo8 8   8 8   8   8   8 'o8 8  "8
    # 8     8 "o, 'ooo' 'ooo' 8 "o, 8   8 8   8 8   8 oo8oo 8  '8 'ooo'
    #
    # anything that requires a build system also probably requires
    # a custom nix development shell, so only include compilers, interpreters,
    # formatters, linters and other useful tools here.
    ##################################################
    # general utils
    git
    license-cli
    direnv
    # c
    gcc
    clang
    clang-tools
    # nix
    nix-tree
    nixfmt-rfc-style
    nixpkgs-review
    nixpkgs-hammering
    nix-prefetch
    nil
    nix-index
    nix-output-monitor
    # shellscripting
    shellcheck
    shfmt
    # rust
    cargo
    rustc
    rustfmt
    clippy
    rustup
    # haskell
    ghc
    # go
    go
    # python
    ruff
    (python3.withPackages (
      python-packages: with python-packages; [
        # data sciency stuff
        scipy
        numpy
        matplotlib
        pandas
        astropy
        ipython
        # linter and formatter
        mypy
        # other
        distro
        pygobject3
        mutagen
        pynput
        # pyppeteer - broken
        requests
        termcolor
        flake8
        readchar
        # needed for eduroam setup
        dbus-python
        # ctf
        pwntools
      ]
    ))
  ];
}
