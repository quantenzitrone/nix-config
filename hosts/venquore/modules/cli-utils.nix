{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    # file-managing
    fdupes
    file
    jmtpfs
    ntfs3g
    ripgrep
    ## office
    pandoc
    pdftk
    python3Packages.weasyprint
    ## media
    ffmpeg_6
    imagemagick
    libmediainfo
    mediainfo
    mnamer
    scour
    yt-dlp
    ## archive
    unar
    unzip
    # fun
    aalib
    asciiquarium-transparent
    # cava
    cbonsai
    cli-visualizer
    cmatrix
    doge
    figlet
    fortune
    hollywood
    lavat
    lolcat
    neo-cowsay
    onefetch
    pipes-rs
    sl
    sssnake
    toilet
    ufetch-fish
    # system utils
    firejail
    glib
    glxinfo
    inxi
    killall
    libinput
    lm_sensors
    lshw
    pciutils
    powermenu-desktop-files
    # helpers for graphical programs
    devour
    libnotify
    toggle-touchpad
    # misc
    openssl
    xdg-user-dirs
    xdg-utils
    ttyper
    wormhole-rs
  ];
}
