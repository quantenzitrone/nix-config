{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    # music library manager
    beets
    # music file utils
    mp3val
    mp4v2
    rsgain
    opustags
    opusTools
    flac
    # to analyze music
    spek
    chromaprint
    # sources of music
    nicotine-plus
    streamrip
    yt-dlp
  ];
}
