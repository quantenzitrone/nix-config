{ pkgs, ... }:
{
  # applications
  ## utilities
  programs.firejail.enable = true;
  programs.gamemode.enable = true;
  ## launchers
  programs.steam.enable = true;

  environment.variables = {
    # fix steam tray icon
    STEAM_RUNTIME_PREFER_HOST_LIBRARIES = "1";
  };

  environment.systemPackages = with pkgs; [
    # utilities
    mangohud
    steam-run-free
    steamtinkerlaunch
    comet-gog
    # launchers
    heroic
    lutris
    # emulators / translation layers
    wine
    wine64
    ppsspp
    # rpcs3 - broken
    # standalone games
    prismlauncher # minecraft
    superTux
    superTuxKart
    extremetuxracer
    osu-lazer
  ];
}
