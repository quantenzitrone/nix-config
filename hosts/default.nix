{ inputs }:
let
  inherit (inputs.nixpkgs) lib;
  inherit (builtins) readDir;
  inherit (lib)
    pathIsRegularFile
    filterAttrs
    mapAttrs
    nixosSystem
    readFile
    ;
in
readDir ./.
|> filterAttrs (n: _: pathIsRegularFile ./${n}/configuration.nix && pathIsRegularFile ./${n}/system)
|> mapAttrs (
  n: _:
  nixosSystem {
    system = readFile ./${n}/system;
    modules = [
      ./${n}/configuration.nix
      { networking.hostName = n; }
    ];
    specialArgs = {
      inherit inputs;
    };
  }
)
