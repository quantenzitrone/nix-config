{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [ diff-so-fancy ];
  programs.git = {
    enable = true;
    config = {
      # me
      user = {
        name = "Quantenzitrone";
        email = "general@dev.quantenzitrone.eu";
      };
      # aliases
      alias = {
        st = "status";
      };
      # cosmetic and behavior tweaks
      core.pager = "diff-so-fancy | less --tabs=4 -R";
      init.defaultBranch = "main";
      interactive.diffFilter = "diff-so-fancy --patch";
      log.date = "format:%c";
      push.autoSetupRemote = true;
    };
  };
}
