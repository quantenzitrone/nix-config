{
  definitions.theme = {
    background = "000C";
    foreground = "fff";
    foreground1 = "999";
    themeType = "dark";

    termcolors.normal = {
      black = "000000";
      white = "ffffff";
      red = "DE5D6E";
      yellow = "FF9470";
      green = "76A85D";
      cyan = "64B5A7";
      blue = "5890F8";
      magenta = "C173D1";
    };
    termcolors.bright = {
      black = "222222";
      white = "ffffff";
      red = "FA7883";
      yellow = "FFC387";
      green = "98C379";
      cyan = "8AF5FF";
      blue = "6BB8FF";
      magenta = "E799FF";
    };
  };
}
