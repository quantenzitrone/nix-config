{
  definitions.theme = {
    background = "000C";
    foreground = "fff";
    themeType = "dark";

    termcolors.normal = {
      red = "f05";
      yellow = "fa0";
      green = "5f0";
      cyan = "0fa";
      blue = "05f";
      magenta = "a0f";
    };
  };
}
