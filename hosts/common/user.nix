{ config, pkgs, ... }:
{
  # define user name via custom module
  definitions.user.name = "zitrone";

  # immutable users so the password doesn't change
  users.mutableUsers = false;

  # use fish as default shell and enable fish
  users.defaultUserShell = pkgs.fish;
  programs.fish.enable = true;

  # define the user
  users.users.${config.definitions.user.name} = {
    uid = 1000;
    description = "quantenzitrone";
    isNormalUser = true;
    createHome = true;
    hashedPassword = "$6$96I31hnopbPjNJOd$M10GoEG0.rvcu1Obi0CyB/2NrXUi4RizVLdOARbVMlqZBSuDaYi43gnDiJqbKnVj1.IM15munPWzEZ9uohu2z0";
    extraGroups = [
      "wheel"
      "video"
      "networkmanager"
      "audio"
      "input"
      "pipewire"
      "libvirtd"
      "ydotool"
      "plugdev"
    ];
  };

  # trust wheel users
  nix.settings.trusted-users = [ "@wheel" ];

  # root user password
  users.users.root.hashedPassword = "$6$96I31hnopbPjNJOd$M10GoEG0.rvcu1Obi0CyB/2NrXUi4RizVLdOARbVMlqZBSuDaYi43gnDiJqbKnVj1.IM15munPWzEZ9uohu2z0";
}
