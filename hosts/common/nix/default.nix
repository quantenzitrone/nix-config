{ inputs, lib, ... }:
let
  mkSubstitutersWithKeys =
    attrs:
    assert builtins.isAttrs attrs;
    {
      nix.settings = {
        substituters = lib.mapAttrsToList (name: _: "https://${name}") attrs;
        trusted-public-keys = lib.mapAttrsToList (
          name: value:
          assert builtins.isString value;
          "${name}${value}"
        ) attrs;
      };
    };
in
{
  imports = [
    inputs.programsdb.nixosModules.programs-sqlite
    (mkSubstitutersWithKeys {
      # add binary cache for nix-community projects
      "nix-community.cachix.org" = "-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=";
      "cosmic.cachix.org" = "-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE=";
    })
  ];

  nix = {
    # enable flakes and related
    settings.extra-experimental-features = [
      "flakes"
      "nix-command"
      "pipe-operator"
      "no-url-literals"
    ];

    # disable global registry, bc it is imparative and uncontrollably updated
    settings.flake-registry = "";

    extraOptions = ''
      repl-overlays = ${./repl-overlays.nix}
    '';

    # map the flake inputs to the system flake registry
    registry = lib.attrsets.mapAttrs' (name: flake: {
      inherit name;
      value = {
        inherit flake;
      };
    }) inputs;

    # replace channel with NIX_PATH
    channel.enable = true;
    nixPath = lib.mapAttrsToList (name: value: "${name}=${value}") inputs;
  };

  # configure command-not-found to work with flakes
  programs-sqlite.enable = true;
}
