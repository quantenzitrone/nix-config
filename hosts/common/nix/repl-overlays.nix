{ currentSystem, ... }:
final: prev:
let
  optionalAttrs = predicate: attrs: if predicate then attrs else { };
in
optionalAttrs (prev ? legacyPackages && prev.legacyPackages ? ${currentSystem}) {
  pkgs = prev.legacyPackages.${currentSystem};
}
